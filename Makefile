NAME     = genAI
LIBNN    = libneuronnetwork

CC       = g++
CPPFLAGS += -Wall -Wextra -pedantic -Werror -o3 -std=c++11
CPPFLAGS += -I ./ -I ./class -I ./$(LIBNN)
CPPFLAGS += -I /usr/local/include

LDFLAGS  += -pthread

SRC      = \
  main.cpp \
  class/GenAI.cpp \
  class/Game.cpp \
  class/SFMLGame.cpp \
  class/BoardValue.cpp \
  class/Board.cpp \
  class/Player.cpp \
  class/Prob.cpp \
  class/LoadingDisplay.cpp \
  class/Arg.cpp \
  class/SFMLGraphicRender.cpp \
  class/GenNeuronNetwork.cpp

OBJ      = $(SRC:.cpp=.o)

LIBSFML  = -L /usr/local/lib -lsfml-graphics -lsfml-window -lsfml-system

RM       = rm -f

all: libnn $(NAME)

libnn:
	make -C $(LIBNN)
	mv $(LIBNN)/$(LIBNN).a ./
	mv $(LIBNN)/$(LIBNN).so ./

$(NAME): $(OBJ)
	$(CC) $(CPPFLAGS) $(LDFLAGS) $(LIBSFML) $(OBJ) -o $(NAME) $(LIBNN).a

clean:
	make -C $(LIBNN)/ clean
	$(RM) $(LIBNN).a
	$(RM) $(LIBNN).so
	$(RM) $(OBJ)

fclean: clean
	make -C $(LIBNN)/ fclean
	$(RM) $(NAME)

re: fclean all

.PHONY   = all $(LIBNN) $(NAME) clean fclean re
