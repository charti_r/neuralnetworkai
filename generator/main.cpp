#include <iostream>
#include <fstream>
#include <string>

#include "GenNeuronNetwork.hpp"

int help(int ret = 1)
{
  std::cout << "generator [[i/h/o]((s)/[t]) nb] [...] (-w [nb]) (-t [nb]) (-l [nb]) (filename)" << std::endl;
  std::cout << "\t" << "[[i/h/o]((s)/[t]) nb] [...] - select neuron types and numbers" << std::endl;
  std::cout << "\t\t" << "i: input" << std::endl;
  std::cout << "\t\t" << "h: hidden" << std::endl;
  std::cout << "\t\t" << "o: output" << std::endl;
  std::cout << "\t\t" << "s: simple" << std::endl;
  std::cout << "\t\t" << "t: threshold" << std::endl;
  std::cout << "\t" << "-w - choose default neuron weight limit" << std::endl;
  std::cout << "\t" << "-t - choose default neuron threshold limit" << std::endl;
  std::cout << "\t" << "-l - choose default neuron link weight limit" << std::endl;
  return ret;
}

bool isstrnum(const std::string & str)
{
  for (unsigned int i = 0; i < str.size(); ++i)
    if (std::isdigit(str[i]) == false)
      return false;
  return true;
}

int main(int ac, char ** av)
{
  GenNeuronNetwork generator;
  std::vector<GenNeuronNetwork::ParamLayer> params;
  std::map<std::string, std::string> map;
  std::string filepath;
  unsigned int weight = 200, threshold = 100, linkWeight = 200;
  int i;

  map["is"] = "input.simple";
  map["it"] = "input.threshold";
  map["hs"] = "hidden.simple";
  map["ht"] = "hidden.threshold";
  map["os"] = "output.simple";
  map["ot"] = "output.threshold";

  if (ac < 3)
    return help();
  i = 1;
  while (i < (ac - 1))
  {
    std::string arg(av[i]);
    int nb;

    if (arg[0] == '-')
    {
      if (arg.size() != 2)
        return help();
    }
    else
    {
      if (arg.size() > 2)
        return help();
      if (arg.size() == 1)
        arg += 's';
      if (map.find(arg) == map.end())
        return help();
    }

    if (!av[++i])
      return help();
    if (isstrnum(av[i]) == false)
      return help();
    nb = std::stoi(av[i]);

    if (arg[0] == '-')
    {
      switch (arg[1])
      {
        case 'w':
          weight = static_cast<unsigned int>(nb);
        break;
        case 't':
          threshold = static_cast<unsigned int>(nb);
        break;
        case 'l':
          linkWeight = static_cast<unsigned int>(nb);
        break;
        default:
          return help();
        break;
      }
    }
    else
      params.push_back(GenNeuronNetwork::ParamLayer(map[arg], nb));
    ++i;
  }

  if (i < ac)
  {
    filepath = av[i];
    if (filepath.find(".") == std::string::npos)
      filepath += ".nnm";
  }
  else
  {
    for (unsigned int j = 0; j < params.size(); ++j)
    {
      if (j)
        filepath += (params[j - 1].type.substr(0, params[j - 1].type.find(".")) == params[i].type.substr(0, params[j].type.find("."))) ? '-' : '_';
      filepath += std::to_string(params[j].nb);
    }
    filepath += ".nnm";
  }

  if (generator.gen(params, weight, threshold, linkWeight) == false)
    {
      std::cout << "gen failed" << std::endl;
      return 1;
    }
  if (generator.writeToFile(filepath) == false)
    {
      std::cout << "fail to save into file" << std::endl;
      return 1;
    }
  return 0;
}
