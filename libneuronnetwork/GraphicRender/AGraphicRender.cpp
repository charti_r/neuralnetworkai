#include "AGraphicRender.hpp"

AGraphicRender::AGraphicRender(const NeuronNetwork & network, unsigned int width, unsigned int height)
  : _network(network)
  , _width(width)
  , _height(height)
{
  updateSize();
}

AGraphicRender::~AGraphicRender()
{

}

void AGraphicRender::updateSize()
{
  const std::vector<NeuronLayer> & layers = _network.getNeuronLayers();

  _layerW = _width / (layers.size() ? layers.size() : 1);
  _layerH.resize(layers.size());

  unsigned int iMin = 0;
  for (unsigned int i = 0; i < layers.size(); ++i)
    {
      _layerH[i] = static_cast<float>(_height) / (layers[i].size() ? layers[i].size() : 1);
      if (_layerH[iMin] > _layerH[i])
        iMin = i;
    }
  _neuronSize = (_layerW < _layerH[iMin] ? _layerW : _layerH[iMin]) * 0.8f;
}

void AGraphicRender::updateSize(unsigned int width, unsigned int height)
{
  _width = width;
  _height = height;
  updateSize();
}

void AGraphicRender::setNetwork(const NeuronNetwork & network)
{
  const_cast<NeuronNetwork &>(_network) = network;
}

void AGraphicRender::setWidth(unsigned int width)
{
  _width = width;
}

void AGraphicRender::setHeight(unsigned int height)
{
  _height = height;
}

unsigned int AGraphicRender::getWidth() const
{
  return _width;
}

unsigned int AGraphicRender::getHeight() const
{
  return _height;
}
