#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include "NeuronNetwork.hpp"
#include "SFMLGraphicRender.hpp"

int help(int ret = 1)
{
  std::cout << "network_render [file]" << std::endl;
  return ret;
}

int main(int ac, char ** av)
{
  NeuronNetwork network;

  if (ac != 2)
    return help();
  if (network.readFromFile(av[1]) == false)
    return help();

  SFMLGraphicRender render(network, 1250, 750);
  sf::RenderWindow win(sf::VideoMode(1250, 750), "NeuronNetwork");
  while (win.isOpen())
  {
    sf::Event event;

    while (win.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
        win.close();
      if (event.type == sf::Event::Resized)
      {
        win.setView(sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height)));
        render.updateSize(event.size.width, event.size.height);
      }
    }

    network.run();
    if (render.genImage(&win) == false)
    {
      win.close();
      return 1;
    }
    win.display();
  }
  return 0;
}
