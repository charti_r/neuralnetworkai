#ifndef AGRAPHICRENDER_HPP_
# define AGRAPHICRENDER_HPP_

# include "Network/NeuronNetwork.hpp"

class AGraphicRender
{
public:
  explicit AGraphicRender(const NeuronNetwork & network, unsigned int width = 100, unsigned int height = 100);
  AGraphicRender(const AGraphicRender & o) = delete;
  AGraphicRender & operator=(const AGraphicRender & o) = delete;
  virtual ~AGraphicRender();

  virtual bool    genImage(void * img) = 0;
  void      updateSize();
  void      updateSize(unsigned int width, unsigned int height);
  void       setNetwork(const NeuronNetwork & network);

  void                  setWidth(unsigned int width);
  void                  setHeight(unsigned int height);

  unsigned int          getWidth() const;
  unsigned int          getHeight() const;

protected:
  const NeuronNetwork &    _network;
  unsigned int            _width, _height;
  float                   _layerW;
  std::vector<float>      _layerH;
  unsigned int            _neuronSize;
};

#endif // !AGRAPHICRENDER_HPP_
