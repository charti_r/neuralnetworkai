#include "NeuronLayer.hpp"

NeuronLayer::NeuronLayer()
  : _neurons()
{

}

NeuronLayer::NeuronLayer(const NeuronLayer & o)
  : _neurons(o._neurons)
{

}

NeuronLayer & NeuronLayer::operator=(const NeuronLayer & o)
{
  if (this == &o)
    return *this;
  _neurons = o._neurons;
  return *this;
}

NeuronLayer::~NeuronLayer()
{

}

void NeuronLayer::run()
{
  for (auto it = _neurons.begin(); it != _neurons.end(); ++it)
    (*it)->run();
}

void NeuronLayer::reset()
{
  for (auto it = _neurons.begin(); it != _neurons.end(); ++it)
    (*it)->reset();
}

std::list<ANeuron *> & NeuronLayer::getNeurons()
{
  return _neurons;
}

const std::list<ANeuron *> & NeuronLayer::getNeurons() const
{
  return _neurons;
}

unsigned int NeuronLayer::size() const
{
  return _neurons.size();
}

std::string NeuronLayer::getType() const
{
  return _neurons.front()->getLayerType();
}

void NeuronLayer::addNeuron(ANeuron * neuron)
{
  _neurons.push_back(neuron);
}

void NeuronLayer::clear()
{
  _neurons.clear();
}
