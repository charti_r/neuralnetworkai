#ifndef NEURONFACTORY_HPP_
# define NEURONFACTORY_HPP_

# include <string>
# include <map>

# include "Neuron/ANeuron.hpp"
# include "Neuron/ANeuronInput.hpp"
# include "Neuron/ANeuronHidden.hpp"
# include "Neuron/ANeuronOutput.hpp"
# include "Neuron/NeuronSimple.hpp"
# include "Neuron/NeuronThreshold.hpp"

class NeuronFactory
{
public:
  typedef ANeuron * (NeuronFactory::*FunctionPtr)();

public:
  NeuronFactory();
  NeuronFactory(const NeuronFactory & o);
  NeuronFactory & operator=(const NeuronFactory & o);
  virtual ~NeuronFactory();

  ANeuron * create(const std::string & type);

protected:
  ANeuron * createInputSimple();
  ANeuron * createInputThreshold();
  ANeuron * createHiddenSimple();
  ANeuron * createHiddenThreshold();
  ANeuron * createOutputSimple();
  ANeuron * createOutputThreshold();

protected:
  std::map<std::string, FunctionPtr> _map;
};

#endif //!NEURONFACTORY_HPP_
