#ifndef NEURONLAYER_HPP_
# define NEURONLAYER_HPP_

# include "Neuron/ANeuron.hpp"

class NeuronLayer
{
public:
  NeuronLayer();
  NeuronLayer(const NeuronLayer & o);
  NeuronLayer & operator=(const NeuronLayer & o);
  virtual ~NeuronLayer();

  void run();
  void reset();

  std::list<ANeuron *> & getNeurons();
  const std::list<ANeuron *> & getNeurons() const;
  unsigned int size() const;
  std::string getType() const;

  void addNeuron(ANeuron * neuron);
  void clear();

protected:
  std::list<ANeuron *> _neurons;
};

#endif // !NEURONLAYER_HPP_
