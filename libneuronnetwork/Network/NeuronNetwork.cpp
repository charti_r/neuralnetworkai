#include "NeuronNetwork.hpp"

NeuronNetwork::NeuronNetwork()
  : _isInit(false)
{

}

NeuronNetwork::NeuronNetwork(const NeuronNetwork & o)
  : _isInit(false)
  , _neurons(o._neurons)
  , _layers(o._layers)
{

}

NeuronNetwork & NeuronNetwork::operator=(const NeuronNetwork & o)
{
  if (this == &o)
    return *this;
  _isInit = o._isInit;
  _neurons = o._neurons;
  _layers = o._layers;
  return *this;
}

NeuronNetwork::~NeuronNetwork()
{
  clear();
}

bool NeuronNetwork::readFromFile(const std::string & filepath)
{
  std::ifstream ifs;

  ifs.open(filepath.c_str());
  if (!ifs)
    return false;
  *this << ifs;
  ifs.close();
  return true;
}

bool NeuronNetwork::writeToFile(const std::string & filepath) const
{
  std::ofstream ofs;

  ofs.open(filepath.c_str());
  if (!ofs)
    return false;
  *this >> ofs;
  ofs.close();
  return true;
}

void NeuronNetwork::operator<<(std::istream & is)
{
  NeuronFactory neuronFactory;
  unsigned int size;
  std::string line;

  clear();
  if (std::getline(is, line))
    size = std::stoll(line);
  while (std::getline(is, line))
  {
    ANeuron * neuron = 0;
    const unsigned int typePos = line.find(',');

    if ((neuron = neuronFactory.create(line.substr(0, typePos))) == 0)
      return;

    *neuron << line.substr(typePos + 1);
    _neurons.push_back(neuron);
  }
  _isInit = build(size);
}

void NeuronNetwork::operator>>(std::ostream & os) const
{
  if (_isInit)
  {
    os << _layers.size() << std::endl;
    for (auto it = _neurons.begin(); it != _neurons.end(); ++it)
      **it >> os;
  }
}

void NeuronNetwork::operator<<(const std::string & is)
{
  std::stringstream ss;

  ss.str(is);
  *this << ss;
}

void NeuronNetwork::operator>>(std::string & os) const
{
  std::stringstream ss;

  *this >> ss;
  os = ss.str();
}

std::list<ANeuron *> & NeuronNetwork::getNeuronList()
{
  return _neurons;
}

const std::list<ANeuron *> & NeuronNetwork::getNeuronList() const
{
  return _neurons;
}

std::vector<NeuronLayer> & NeuronNetwork::getNeuronLayers()
{
  return _layers;
}

const std::vector<NeuronLayer> & NeuronNetwork::getNeuronLayers() const
{
  return _layers;
}

const ANeuronOutput * NeuronNetwork::run()
{
  const ANeuronOutput * max = 0;

  if (!_isInit)
    return 0;

  for (auto it = _layers.begin(); it != _layers.end(); ++it)
    it->run();

  const std::list<ANeuron *> & neurons = _layers[_layers.size() - 1].getNeurons();
  for (auto it = neurons.begin(); it != neurons.end(); ++it)
    if ((*it)->getValue() > (max ? max->getValue() : 0))
      max = static_cast<ANeuronOutput *>(*it);
  return max;
}

void NeuronNetwork::reset()
{
  for (auto it = _layers.begin(); it != _layers.end(); ++it)
    it->reset();
}

void NeuronNetwork::clear()
{
  for (auto it = _neurons.begin(); it != _neurons.end(); ++it)
    delete *it;
  _neurons.clear();
  _layers.clear();
  _layers.resize(0);
  _isInit = false;
}

bool NeuronNetwork::build(unsigned int size)
{
  _layers.resize(size);
  for (auto itn = _neurons.begin(); itn != _neurons.end(); ++itn)
    {
      ANeuron::SaveData & data = (*itn)->getData();

      _layers[data.layer].addNeuron(*itn);
      for (auto itl = data.link.begin(); itl != data.link.end(); ++itl)
      {
        bool stop = false;

        for (auto it = _neurons.begin(); it != _neurons.end() && !stop; ++it)
          if ((*it)->getData().id == itl->id)
          {
            itl->ptr = *it;
            stop = true;
          }
        if (itl->ptr == 0) //Cant find NeuronID to link with it
          return false;
      }
    }
  return true;
}
