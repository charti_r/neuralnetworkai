#include "NeuronFactory.hpp"

NeuronFactory::NeuronFactory()
{
  _map["input.simple"] = &NeuronFactory::createInputSimple;
  _map["input.threshold"] = &NeuronFactory::createInputThreshold;
  _map["hidden.simple"] = &NeuronFactory::createHiddenSimple;
  _map["hidden.threshold"] = &NeuronFactory::createHiddenThreshold;
  _map["output.simple"] = &NeuronFactory::createOutputSimple;
  _map["output.threshold"] = &NeuronFactory::createOutputThreshold;
}

NeuronFactory::NeuronFactory(const NeuronFactory & o)
  : _map(o._map)
{

}

NeuronFactory & NeuronFactory::operator=(const NeuronFactory & o)
{
  if (this == &o)
    return *this;
  _map = o._map;
  return *this;
}

NeuronFactory::~NeuronFactory()
{

}

ANeuron * NeuronFactory::create(const std::string & type)
{
  const FunctionPtr ptr = _map[type];

  return ptr ? (this->*ptr)() : 0;
}

ANeuron * NeuronFactory::createInputSimple()
{
  return new NeuronInputSimple();
}

ANeuron * NeuronFactory::createInputThreshold()
{
  return new NeuronInputThreshold();
}

ANeuron * NeuronFactory::createHiddenSimple()
{
  return new NeuronHiddenSimple();
}

ANeuron * NeuronFactory::createHiddenThreshold()
{
  return new NeuronHiddenThreshold();
}

ANeuron * NeuronFactory::createOutputSimple()
{
  return new NeuronOutputSimple();
}

ANeuron * NeuronFactory::createOutputThreshold()
{
  return new NeuronOutputThreshold();
}
