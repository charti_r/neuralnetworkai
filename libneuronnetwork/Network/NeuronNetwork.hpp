#ifndef NEURONNETWORK_HPP_
# define NEURONNETWORK_HPP_

# include <iostream>
# include <fstream>
# include <string>
# include <vector>
# include <list>
# include <sstream>

# include "NeuronFactory.hpp"
# include "NeuronLayer.hpp"
# include "Neuron/ANeuron.hpp"

class NeuronNetwork
{
public:
  NeuronNetwork();
  NeuronNetwork(const NeuronNetwork & o);
  NeuronNetwork & operator=(const NeuronNetwork & o);
  virtual ~NeuronNetwork();

  bool readFromFile(const std::string & filepath);
  bool writeToFile(const std::string & filepath) const;

  void operator<<(std::istream & is);
  void operator>>(std::ostream & os) const;
  void operator<<(const std::string & is);
  void operator>>(std::string & os) const;

  std::list<ANeuron *> &            getNeuronList();
  const std::list<ANeuron *> &      getNeuronList() const;
  std::vector<NeuronLayer> &        getNeuronLayers();
  const std::vector<NeuronLayer> &  getNeuronLayers() const;

  //Return activated output neuron
  const ANeuronOutput * run();
  void                  reset();
  void                  clear();

protected:
  bool build(unsigned int size);

protected:
  bool _isInit;

  std::list<ANeuron *> _neurons;
  std::vector<NeuronLayer> _layers;
};

template <typename T>
T & operator<<(T & os, const NeuronNetwork & nn)
{
  nn >> os;
  return os;
}

template <typename T>
const T & operator>>(T & is, NeuronNetwork & nn)
{
  nn << is;
  return is;
}

#endif // !NEURONNETWORK_HPP_
