#ifndef GENETICALGO_HPP_
# define GENETICALGO_HPP_

# include <iostream>
# include <vector>
# include <algorithm>
# include <cstdlib>
# include <ctime>

class GeneticAlgo
{
public:
  explicit GeneticAlgo(float crossPercent = 0.05, float mutPercent = 0.015);
  GeneticAlgo(const GeneticAlgo & o);
  GeneticAlgo & operator=(const GeneticAlgo & o);
  virtual ~GeneticAlgo();

  void setCrossPercent(float crossPercent);
  void setMutPercent(float mutPercent);

  float getCrossPercent() const;
  float getMutPercent() const;

  template <typename T>
  static std::vector<bool> getBinary(const T & data)
  {
    constexpr unsigned int size = sizeof(T) * 8;
    std::vector<bool> arr(size);

    for (unsigned int b = 0; b < size; ++b)
      arr[b] = (data >> b) & 1;
    return arr;
  }

  template <typename T>
  static std::vector<bool> getBinary(const std::vector<T> & data)
  {
    constexpr unsigned int size = sizeof(T) * 8;
    std::vector<bool> arr(data.size() * size);

    for (unsigned int i = 0; i < data.size(); ++i)
      for (unsigned int b = 0; b < size; ++b)
        arr[(i * size) + b] = (data[i] >> b) & 1;
    return arr;
  }

  template <typename T>
  static T setBinary(const std::vector<bool> & arr, unsigned int & pos)
  {
    constexpr unsigned int size = sizeof(T) * 8;
    T value = 0;

    for (unsigned int b = 0; b < size; ++b)
      value += static_cast<unsigned int>(arr[pos + b]) << b;
    pos += size;
    return value;
  }

  static std::vector<bool> bincat(const std::vector<bool> & data1, const std::vector<bool> & data2);
  void crossover(const std::vector<bool> & data1, const std::vector<bool> & data2, std::vector<bool> & res1, std::vector<bool> & res2) const;

protected:
  void genPosArr(std::vector<unsigned int> & posArr, float percent, unsigned int size) const;

protected:
  float _crossPercent;
  float _mutPercent;
};

std::vector<bool> operator+(const std::vector<bool> & data1, const std::vector<bool> & data2);
std::vector<bool> & operator+=(std::vector<bool> & data1, const std::vector<bool> & data2);

#endif // !GENETICALGO_HPP_
