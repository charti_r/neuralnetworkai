#include <iostream>
#include <vector>

#include "GeneticAlgo.hpp"

template <typename T>
std::ostream & operator<<(std::ostream & os, const std::vector<T> & arr)
{
  for (unsigned int i = 0; i < arr.size(); ++i)
    os << arr[i];
  return os;
}

int main()
{
  std::vector<bool> data1, data2;
  std::vector<bool> res1, res2;

  int weight1 = 50, weight2 = 30;
  int linkWeight1 = 20, linkWeight2 = 40;
  unsigned int threshold1 = 20, threshold2 = 15;

  data1 += GeneticAlgo::getBinary(weight1);
  data1 += GeneticAlgo::getBinary(linkWeight1);
  data1 += GeneticAlgo::getBinary(threshold1);

  data2 += GeneticAlgo::getBinary(weight2);
  data2 += GeneticAlgo::getBinary(linkWeight2);
  data2 += GeneticAlgo::getBinary(threshold2);

  GeneticAlgo::crossover(data1, data2, res1, res2);

  std::cout << "data1 : " << data1 << std::endl;
  std::cout << "data2 : " << data2 << std::endl;
  std::cout << "res1  : " << res1 << std::endl;
  std::cout << "res2  : " << res2 << std::endl;
  return 0;
}
