#include "GeneticAlgo.hpp"

GeneticAlgo::GeneticAlgo(float crossPercent, float mutPercent)
  : _crossPercent(crossPercent)
  , _mutPercent(mutPercent)
{
  std::srand(std::time(0));
}

GeneticAlgo::GeneticAlgo(const GeneticAlgo & o)
  : _crossPercent(o._crossPercent)
  , _mutPercent(o._mutPercent)
{

}

GeneticAlgo & GeneticAlgo::operator=(const GeneticAlgo & o)
{
  if (this == &o)
    return *this;
  _crossPercent = o._crossPercent;
  _mutPercent = o._mutPercent;
  return *this;
}

GeneticAlgo::~GeneticAlgo()
{

}

void GeneticAlgo::setCrossPercent(float crossPercent)
{
  _crossPercent = crossPercent;
}

void GeneticAlgo::setMutPercent(float mutPercent)
{
  _mutPercent = mutPercent;
}

float GeneticAlgo::getCrossPercent() const
{
  return _crossPercent;
}

float GeneticAlgo::getMutPercent() const
{
  return _mutPercent;
}

std::vector<bool> GeneticAlgo::bincat(const std::vector<bool> & data1, const std::vector<bool> & data2)
{
  std::vector<bool> res(data1.size() + data2.size());
  unsigned int i, j;

  i = 0;
  while (i < data1.size())
  {
    res[i] = data1[i];
    ++i;
  }
  j = 0;
  while (i < res.size() && j < data2.size())
  {
    res[i] = data2[j];
    ++i;
    ++j;
  }
  return res;
}

void GeneticAlgo::crossover(const std::vector<bool> & data1, const std::vector<bool> & data2, std::vector<bool> & res1, std::vector<bool> & res2) const
{
  std::vector<unsigned int> crossPos, mutPos;
  unsigned int minSize, maxSize;
  unsigned int i, iData, iCross, iMut;

  minSize = data1.size() > data2.size() ? data2.size() : data1.size();
  maxSize = data1.size() > data2.size() ? data1.size() : data2.size();
  res1.resize(maxSize);
  res2.resize(maxSize);
  genPosArr(crossPos, _crossPercent, minSize);
  genPosArr(mutPos, _mutPercent, maxSize);

  iData = std::rand() % 2;
  iCross = 0;
  for (i = 0; i < minSize; ++i)
  {
    if (iCross < crossPos.size() && crossPos[iCross] == i)
    {
      iData = !iData;
      ++iCross;
    }
    res1[i] = iData ? data1[i] : data2[i];
    res2[i] = iData ? data2[i] : data1[i];
  }
  while (i < maxSize)
  {
    res1[i] = data1.size() > data2.size() ? data1[i] : data2[i];
    res2[i] = data1.size() > data2.size() ? data1[i] : data2[i];
    ++i;
  }

  if (mutPos.size() == 0)
    return;

  // for (i = 0; i < mutPos.size(); ++i)
  //   std::cout << "[" << i << "] = " << mutPos[i] << std::endl;

  iMut = 0;
  for (i = 0; i < maxSize; ++i)
  {
    while (mutPos[iMut] > i && i < maxSize)
    ++i;
    if (i < maxSize)
    {
      res1[i] = !res1[i];
      res2[i] = !res2[i];
      ++iMut;
    }
  }
}

void GeneticAlgo::genPosArr(std::vector<unsigned int> & posArr, float percent, unsigned int size) const
{
  posArr.resize(percent * size, static_cast<unsigned int>(-1));
  for (unsigned int i = 0; i < posArr.size(); ++i)
  {
    posArr[i] = std::rand() % size;
    while (std::find(posArr.begin(), posArr.end(), posArr[i]) == posArr.end())
      posArr[i] = std::rand() % size;
  }
  std::sort(posArr.begin(), posArr.end());
}

std::vector<bool> operator+(const std::vector<bool> & data1, const std::vector<bool> & data2)
{
  return GeneticAlgo::bincat(data1, data2);
}

std::vector<bool> & operator+=(std::vector<bool> & data1, const std::vector<bool> & data2)
{
  unsigned int i = data1.size();

  data1.resize(data1.size() + data2.size());
  for (unsigned int j = 0; j < data2.size(); ++j)
  {
    data1[i] = data2[j];
    ++i;
  }
  return data1;
}
