#ifndef ANEURONHIDDEN_HPP_
# define ANEURONHIDDEN_HPP_

# include "ANeuron.hpp"

class ANeuronHidden : public ANeuron
{
public:
  ANeuronHidden();
  ANeuronHidden(const ANeuronHidden & o);
  ANeuronHidden & operator=(const ANeuronHidden & o);
  virtual ~ANeuronHidden();

  virtual std::string getLayerType() const;
};

#endif // !ANEURONHIDDEN_HPP_
