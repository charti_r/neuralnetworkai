#include "ANeuron.hpp"

ANeuron::Link::Link(unsigned int id_, int weight_)
  : id(id_)
  , weight(weight_)
  , valueSend(0)
  , ptr(0)
{

}

ANeuron::Link::Link(const Link & o)
  : id(o.id)
  , weight(o.weight)
  , valueSend(o.valueSend)
  , ptr(o.ptr)
{

}

ANeuron::Link & ANeuron::Link::operator=(const Link & o)
{
  if (this == &o)
    return *this;
  id = o.id;
  weight = o.weight;
  valueSend = o.valueSend;
  ptr = o.ptr;
  return *this;
}

ANeuron::Link::~Link()
{

}

void ANeuron::Link::operator<<(std::istream & is)
{
  std::string buffer;

  for (unsigned int i = 0; std::getline(is, buffer, '.'); ++i)
  {
    switch (i)
    {
      case 0:
        id = std::stoll(buffer);
      break;

      case 1:
        weight = std::stoll(buffer);
      break;

      default:
        return;
      break;
    }
  }
}

void ANeuron::Link::operator>>(std::ostream & os) const
{
  os << id << '.' << weight;
}

void ANeuron::Link::operator<<(const std::string & is)
{
  std::stringstream ss;

  ss.str(is);
  *this << ss;
}

void ANeuron::Link::operator>>(std::string & os) const
{
  std::stringstream ss;

  *this >> ss;
  os = ss.str();
}

ANeuron::SaveData::SaveData()
  : id(0)
  , weight(0)
  , threshold(0)
  , link()
{

}

ANeuron::SaveData::SaveData(const ANeuron::SaveData & o)
  : id(o.id)
  , weight(o.weight)
  , threshold(o.threshold)
  , link(o.link)
{

}

ANeuron::SaveData & ANeuron::SaveData::operator=(const ANeuron::SaveData & o)
{
  if (this == &o)
    return *this;
  id = o.id;
  weight = o.weight;
  threshold = o.threshold;
  link = o.link;
  return *this;
}

ANeuron::SaveData::~SaveData()
{

}

void ANeuron::SaveData::operator<<(std::istream & is)
{
  std::string buffer;

  for (unsigned int i = 0; std::getline(is, buffer, ','); ++i)
  {
    switch (i)
    {
      case 0:
        id = std::stoll(buffer);
      break;

      case 1:
        layer = std::stoll(buffer);
      break;

      case 2:
        weight = std::stoll(buffer);
      break;

      case 3:
        threshold = std::stoll(buffer);
      break;

      default:
        link.push_back(Link());
        link.back() << buffer;
      break;
    }
  }
}

void ANeuron::SaveData::operator>>(std::ostream & os) const
{
  os << id << ',' << layer << ',' << weight << ',' << threshold;
  for (auto it = link.begin(); it != link.end(); ++it)
    os << ',' << *it;
}

void ANeuron::SaveData::operator<<(const std::string & is)
{
  std::stringstream ss;

  ss.str(is);
  *this << ss;
}

void ANeuron::SaveData::operator>>(std::string & os) const
{
  std::stringstream ss;

  *this >> ss;
  os = ss.str();
}

std::vector<bool> ANeuron::SaveData::getDNA() const
{
  std::vector<bool> dna;

  dna += GeneticAlgo::getBinary(weight);
  dna += GeneticAlgo::getBinary(threshold);
  for (auto it = link.begin(); it != link.end(); ++it)
    dna += GeneticAlgo::getBinary(it->weight);
  return dna;
}

unsigned int ANeuron::SaveData::setDNA(const std::vector<bool> & dna, unsigned int pos)
{
  weight = GeneticAlgo::setBinary<decltype(weight)>(dna, pos);
  threshold = GeneticAlgo::setBinary<decltype(threshold)>(dna, pos);
  for (auto it = link.begin(); it != link.end(); ++it)
    it->weight = GeneticAlgo::setBinary<decltype(it->weight)>(dna, pos);
  return pos;
}

ANeuron::ANeuron()
  : _data()
  , _value(0)
{

}

ANeuron::ANeuron(const ANeuron & o)
  : _data(o._data)
  , _value(o._value)
{

}

ANeuron & ANeuron::operator=(const ANeuron & o)
{
  if (this == &o)
    return *this;
  _data = o._data;
  _value = o._value;
  return *this;
}

ANeuron::~ANeuron()
{

}

void ANeuron::operator<<(std::istream & is)
{
  is >> _data;
}

void ANeuron::operator>>(std::ostream & os) const
{
  os << getLayerType() << "." << getNeuronType() << "," << _data << std::endl;
}

void ANeuron::operator<<(const std::string & is)
{
  std::stringstream ss;

  ss.str(is);
  *this << ss;
}

void ANeuron::operator>>(std::string & os) const
{
  std::stringstream ss;

  *this >> ss;
  os = ss.str();
}

std::vector<bool> ANeuron::getDNA() const
{
  return _data.getDNA();
}

unsigned int ANeuron::setDNA(const std::vector<bool> & dna, unsigned int pos)
{
  return _data.setDNA(dna, pos);
}

ANeuron::SaveData & ANeuron::getData()
{
  return _data;
}

const ANeuron::SaveData & ANeuron::getData() const
{
  return _data;
}

unsigned int ANeuron::getId() const
{
  return _data.id;
}

unsigned int ANeuron::getLayerId() const
{
  return _data.layer;
}

int ANeuron::getValue() const
{
  return _value;
}

void ANeuron::run()
{
  const float coef = transfer();
  int value;

  if (coef)
    for (auto it = _data.link.begin(); it != _data.link.end(); ++it)
      {
  value = coef * (_value * _data.weight * it->weight);
  it->ptr->sendValue(value);
  it->valueSend = value;
      }
}

void ANeuron::reset()
{
  _value = 0;
}
