#include "ANeuronOutput.hpp"

ANeuronOutput::ANeuronOutput()
  : ANeuron()
  , _outputId(0)
{

}

ANeuronOutput::ANeuronOutput(const ANeuronOutput & o)
  : ANeuron(o)
  , _outputId(o._outputId)
{

}

ANeuronOutput & ANeuronOutput::operator=(const ANeuronOutput & o)
{
  if (this == &o)
    return *this;
  ANeuron::operator=(o);
  _outputId = o._outputId;
  return *this;
}

ANeuronOutput::~ANeuronOutput()
{

}

std::string ANeuronOutput::getLayerType() const
{
  return std::string("output");
}

unsigned int ANeuronOutput::getOutputId() const
{
  return _outputId;
}

void ANeuronOutput::setOutputId(unsigned int id)
{
  _outputId = id;
}
