#include "ANeuronHidden.hpp"

ANeuronHidden::ANeuronHidden()
  : ANeuron()
{

}

ANeuronHidden::ANeuronHidden(const ANeuronHidden & o)
  : ANeuron(o)
{

}

ANeuronHidden & ANeuronHidden::operator=(const ANeuronHidden & o)
{
  if (this == &o)
    return *this;
  ANeuron::operator=(o);
  return *this;
}

ANeuronHidden::~ANeuronHidden()
{

}

std::string ANeuronHidden::getLayerType() const
{
  return std::string("hidden");
}
