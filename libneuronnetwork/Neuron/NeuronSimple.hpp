#ifndef NEURONSIMPLE_HPP_
# define NEURONSIMPLE_HPP_

# include <string>
# include <cmath>

# include "ANeuronInput.hpp"
# include "ANeuronHidden.hpp"
# include "ANeuronOutput.hpp"

template <class T>
class NeuronSimple : public T
{
public:
  NeuronSimple()
    : T()
  {

  }

  NeuronSimple(const NeuronSimple & o)
    : T(o)
  {

  }

  NeuronSimple & operator=(const NeuronSimple & o)
  {
    if (this == &o)
      return *this;
    T::operator=(o);
    return *this;
  }

  virtual ~NeuronSimple()
  {

  }

  virtual void sendValue(int value) override
  {
    this->_value += value;
  }

  virtual std::string getNeuronType() const override
  {
    return std::string("simple");
  }

  virtual float transfer() override
  {
    // return this->_value > static_cast<int>(this->_data.threshold) ? 1.f : 0.f;
    return 1.f / (1.f + exp(this->_data.threshold - this->_value));
  }
};

typedef NeuronSimple<ANeuronInput> NeuronInputSimple;
typedef NeuronSimple<ANeuronHidden> NeuronHiddenSimple;
typedef NeuronSimple<ANeuronOutput> NeuronOutputSimple;

#endif // !NEURONSIMPLE_HPP_
