#include "ANeuronInput.hpp"

ANeuronInput::ANeuronInput()
  : ANeuron()
{

}

ANeuronInput::ANeuronInput(const ANeuronInput & o)
  : ANeuron(o)
{

}

ANeuronInput & ANeuronInput::operator=(const ANeuronInput & o)
{
  if (this == &o)
    return *this;
  ANeuron::operator=(o);
  return *this;
}

ANeuronInput::~ANeuronInput()
{

}

std::string ANeuronInput::getLayerType() const
{
  return std::string("input");
}

int & ANeuronInput::getRefValue()
{
  return _value;
}
