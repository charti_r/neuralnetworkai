#ifndef NEURONTHRESHOLD_HPP_
# define NEURONTHRESHOLD_HPP_

# include <string>
# include <cmath>

# include "ANeuronInput.hpp"
# include "ANeuronHidden.hpp"
# include "ANeuronOutput.hpp"

template <class T>
class NeuronThreshold : public T
{
public:
  NeuronThreshold()
    : T()
  {

  }

  NeuronThreshold(const NeuronThreshold & o)
    : T(o)
  {

  }

  NeuronThreshold & operator=(const NeuronThreshold & o)
  {
    if (this == &o)
      return *this;
    T::operator=(o);
    return *this;
  }

  virtual ~NeuronThreshold()
  {

  }

  virtual void sendValue(int value) override
  {
    this->_value += value;
  }

  virtual std::string getNeuronType() const override
  {
    return std::string("threshold");
  }

  virtual float transfer() override
  {
    float diff;

    diff = std::abs(static_cast<float>(this->_data.threshold) - this->_value);
    return diff > this->_data.threshold ? 0.f : (this->_data.threshold - diff) / this->_data.threshold;
  }
};

typedef NeuronThreshold<ANeuronInput> NeuronInputThreshold;
typedef NeuronThreshold<ANeuronHidden> NeuronHiddenThreshold;
typedef NeuronThreshold<ANeuronOutput> NeuronOutputThreshold;

#endif // !NEURONTHRESHOLD_HPP_
