#ifndef ANEURON_HPP_
# define ANEURON_HPP_

# include <iostream>
# include <string>
# include <sstream>
# include <list>

# include "Genetic/GeneticAlgo.hpp"

class ANeuron
{
public:
  struct Link
  {
    explicit Link(unsigned int id_ = 0, int weight_ = 0);
    Link(const Link & o);
    Link & operator=(const Link & o);
    virtual ~Link();

    void operator<<(std::istream & is);
    void operator>>(std::ostream & os) const;
    void operator<<(const std::string & is);
    void operator>>(std::string & os) const;

    unsigned int  id;
    int           weight;
    int      valueSend;
    ANeuron *     ptr;
  };

  struct SaveData
  {
    SaveData();
    SaveData(const SaveData & o);
    SaveData & operator=(const SaveData & o);
    virtual ~SaveData();

    void operator<<(std::istream & is);
    void operator>>(std::ostream & os) const;
    void operator<<(const std::string & is);
    void operator>>(std::string & os) const;

    std::vector<bool> getDNA() const;
    unsigned int setDNA(const std::vector<bool> & dna, unsigned int pos);

    unsigned int    id;
    unsigned int    layer;
    int              weight;
    unsigned int    threshold;
    std::list<Link>  link;
  };

public:
  ANeuron();
  ANeuron(const ANeuron & o);
  ANeuron & operator=(const ANeuron & o);
  virtual ~ANeuron();

  void operator<<(std::istream & is);
  void operator>>(std::ostream & os) const;
  void operator<<(const std::string & is);
  void operator>>(std::string & os) const;

  std::vector<bool>  getDNA() const;
  unsigned int       setDNA(const std::vector<bool> & dna, unsigned int pos = 0);

  virtual std::string getNeuronType() const = 0;
  virtual std::string getLayerType() const = 0;
  virtual void        sendValue(int value) = 0;

  SaveData &          getData();
  const SaveData &    getData() const;
  unsigned int        getId() const;
  unsigned int        getLayerId() const;
  int                 getValue() const;
  void                run();
  void                reset();

protected:
  // return value between [0.f; 1.f] || [-1.f; 1.f]
  virtual float       transfer() = 0;

protected:
  SaveData            _data;
  int                 _value;
};

template <typename T>
T & operator<<(T & os, const ANeuron::Link & data)
{
  data >> os;
  return os;
}

template <typename T>
const T & operator>>(T & is, ANeuron::Link & data)
{
  data << is;
  return is;
}

template <typename T>
T & operator<<(T & os, const ANeuron::SaveData & data)
{
  data >> os;
  return os;
}

template <typename T>
const T & operator>>(T & is, ANeuron::SaveData & data)
{
  data << is;
  return is;
}

template <typename T>
T & operator<<(T & os, const ANeuron * neuron)
{
  *neuron >> os;
  return os;
}

template <typename T>
const T & operator>>(T & is, ANeuron * neuron)
{
  *neuron << is;
  return is;
}

#endif // !ANEURON_HPP_
