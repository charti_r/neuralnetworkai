#ifndef ANEURONINPUT_HPP_
# define ANEURONINPUT_HPP_

# include "ANeuron.hpp"

class ANeuronInput : public ANeuron
{
public:
  ANeuronInput();
  ANeuronInput(const ANeuronInput & o);
  ANeuronInput & operator=(const ANeuronInput & o);
  virtual ~ANeuronInput();

  virtual std::string getLayerType() const;
  int & getRefValue();
};

#endif // !ANEURONINPUT_HPP_
