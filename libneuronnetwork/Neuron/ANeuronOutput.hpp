#ifndef ANEURONOUTPUT_HPP_
# define ANEURONOUTPUT_HPP_

# include "ANeuron.hpp"

class ANeuronOutput : public ANeuron
{
public:
  ANeuronOutput();
  ANeuronOutput(const ANeuronOutput & o);
  ANeuronOutput & operator=(const ANeuronOutput & o);
  virtual ~ANeuronOutput();

  virtual std::string getLayerType() const;

  unsigned int getOutputId() const;
  void         setOutputId(unsigned int id);

protected:
  unsigned int _outputId;
};

#endif // !ANEURONOUTPUT_HPP_
