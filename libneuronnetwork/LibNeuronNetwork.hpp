#ifndef LIB_NEURONNETWORK_HPP_
# define LIB_NEURONNETWORK_HPP_

# include "GraphicRender/AGraphicRender.hpp"

# include "Genetic/GeneticAlgo.hpp"

# include "Network/NeuronNetwork.hpp"

# include "Neuron/ANeuron.hpp"
# include "Neuron/ANeuronInput.hpp"
# include "Neuron/ANeuronHidden.hpp"
# include "Neuron/ANeuronOutput.hpp"
# include "Neuron/NeuronSimple.hpp"
# include "Neuron/NeuronThreshold.hpp"

#endif // !LIB_NEURONNETWORK_HPP_
