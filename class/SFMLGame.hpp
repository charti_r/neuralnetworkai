#ifndef GAME_HPP_
# define GAME_HPP_

# include <iostream>
# include <vector>
# include <string>

# include <cstdlib>
# include <ctime>
# include <unistd.h>

# include <SFML/Graphics.hpp>
# include <SFML/Window.hpp>
# include <SFML/System.hpp>

# include "LibNeuronNetwork.hpp"

# include "Game.hpp"
# include "SFMLGraphicRender.hpp"
# include "Board.hpp"
# include "Player.hpp"
# include "NetworkData.hpp"

class SFMLGame : public Game
{
public:
    explicit SFMLGame(sf::RenderWindow & win, unsigned int nbPlayer);
    SFMLGame(const SFMLGame &) = delete;
    SFMLGame & operator=(const SFMLGame &) = delete;
    virtual ~SFMLGame();

protected:
    virtual bool onInit();
    virtual void handleEvent();
    virtual void displayBoard();

private:
    void close();
    void resize(const unsigned int width, const unsigned int height);
    void handleKey(unsigned int keycode);

protected:
    std::vector<AGraphicRender *>    _renders;
    std::vector<sf::RenderTexture *> _wins;
    sf::RenderWindow &               _win;
    bool                             _winIsFocused;
};

#endif // !GAME_HPP_
