#include "GenNeuronNetwork.hpp"

GenNeuronNetwork::ParamLayer::ParamLayer(const std::string & type_, unsigned int nb_)
    : type(type_)
    , nb(nb_)
{
    std::srand(std::time(0));
}

GenNeuronNetwork::ParamLayer::ParamLayer(const ParamLayer & o)
    : type(o.type)
    , nb(o.nb)
{

}

GenNeuronNetwork::ParamLayer & GenNeuronNetwork::ParamLayer::operator=(const ParamLayer & o)
{
    if (this == &o)
        return *this;
    type = o.type;
    nb = o.nb;
    return *this;
}

GenNeuronNetwork::ParamLayer::~ParamLayer()
{

}


GenNeuronNetwork::GenNeuronNetwork()
    : NeuronNetwork()
{

}

GenNeuronNetwork::GenNeuronNetwork(const GenNeuronNetwork & o)
    : NeuronNetwork(o)
{

}

GenNeuronNetwork & GenNeuronNetwork::operator=(const GenNeuronNetwork & o)
{
    if (this == &o)
        return *this;
    NeuronNetwork::operator=(o);
    return *this;
}

GenNeuronNetwork::~GenNeuronNetwork()
{

}


bool GenNeuronNetwork::gen(const std::vector<GenNeuronNetwork::ParamLayer> & params, unsigned int weightLimit, unsigned int thresholdLimit, unsigned int linkWeighLimit)
{
    NeuronFactory neuronFactory;
    unsigned int id, firstLayerId;

    clear();
    id = 0;
    for (unsigned int i = 0; i < params.size(); ++i) {
        firstLayerId = id;
        for (unsigned int j = 0; j < params[i].nb; ++j) {
            ANeuron * neuron = 0;

            if ((neuron = neuronFactory.create(params[i].type)) == 0)
                return false;
            ANeuron::SaveData & data = neuron->getData();

            data.id = id++;
            data.layer = i;
            data.weight = (std::rand() % (weightLimit * 2)) - weightLimit;
            data.threshold = std::rand() % thresholdLimit;
            if ((i + 1) < params.size())
                for (unsigned int l = 0; l < params[i + 1].nb; ++l)
                    data.link.push_back(ANeuron::Link(l + firstLayerId + params[i].nb, (std::rand() % (linkWeighLimit * 2)) - linkWeighLimit));
            _neurons.push_back(neuron);
        }
    }

    _isInit = build(params.size());
    return _isInit;
}

std::vector<bool> GenNeuronNetwork::getDNA() const
{
    std::vector<bool> dna;

    for (auto it = _neurons.begin(); it != _neurons.end(); ++it)
        dna += (*it)->getDNA();
    return dna;
}

bool GenNeuronNetwork::setDNA(const std::vector<ParamLayer> & params, const std::vector<bool> & dna)
{
    NeuronFactory neuronFactory;
    unsigned int id, firstLayerId;
    unsigned int dnaPos = 0;

    clear();
    id = 0;
    for (unsigned int i = 0; i < params.size(); ++i) {
        firstLayerId = id;
        for (unsigned int j = 0; j < params[i].nb; ++j) {
            ANeuron * neuron = 0;

            if ((neuron = neuronFactory.create(params[i].type)) == 0)
                return false;
            ANeuron::SaveData & data = neuron->getData();

            data.id = id++;
            data.layer = i;
            if ((i + 1) < params.size())
                for (unsigned int l = 0; l < params[i + 1].nb; ++l)
                    data.link.push_back(ANeuron::Link(l + firstLayerId + params[i].nb, 0));
            _neurons.push_back(neuron);
        }
    }

    _isInit = build(params.size());
    for (auto it = _neurons.begin(); it != _neurons.end(); ++it)
        dnaPos = (*it)->setDNA(dna, dnaPos);
    return _isInit;
}

std::vector<bool> GenNeuronNetwork::getDNA(const std::string & data)
{
    std::vector<bool> dna;
    std::string line;
    std::stringstream ss0;
    std::stringstream * ss1 = 0;

    ss0.str(data);
    std::getline(ss0, line); // network's width
    while (std::getline(ss0, line)) {
        if (ss1)
            delete ss1;
        ss1 = new std::stringstream;
        ss1->str(line);
        for (unsigned int i = 0; std::getline(*ss1, line, ','); ++i) {
            if (i == 3) // neuron's weight
                dna += GeneticAlgo::getBinary(static_cast<unsigned int>(std::stoll(line)));
            else if (i == 4) // neuron's threshold
                dna += GeneticAlgo::getBinary(static_cast<unsigned int>(std::stoll(line)));
            else if (i >= 5) // neuron's link's weight
                dna += GeneticAlgo::getBinary(static_cast<unsigned int>(std::stoll(line.substr(line.find('.') + 1))));
        }
    }
    return dna;
}

bool GenNeuronNetwork::setDNA(const std::vector<ParamLayer> & params, const std::vector<bool> & dna, std::string & data)
{
    unsigned int id, firstLayerId;
    unsigned int dnaPos = 0;
    std::stringstream ss;

    ss << params.size() << std::endl;
    id = 0;
    for (unsigned int i = 0; i < params.size(); ++i) {
        firstLayerId = id;
        for (unsigned int j = 0; j < params[i].nb; ++j) {
            ANeuron::SaveData neuronData;

            neuronData.id = id++;
            neuronData.layer = i;
            if ((i + 1) < params.size())
                for (unsigned int l = 0; l < params[i + 1].nb; ++l)
                    neuronData.link.push_back(ANeuron::Link(l + firstLayerId + params[i].nb, 0));
            dnaPos = neuronData.setDNA(dna, dnaPos);
            ss << params[i].type << "," << neuronData << std::endl;
        }
    }
    data = ss.str();
    return true;
}
