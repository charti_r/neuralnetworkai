#include "Board.hpp"

Board::Board(unsigned int sizex, unsigned int sizey)
    : Arr2<BoardValue>(sizex, sizey)
{

}

Board::Board(const Board & o)
    : Arr2<BoardValue>(o)
{

}

Board & Board::operator=(const Board & o)
{
    if (this == &o)
        return *this;
    Arr2<BoardValue>::operator=(o);
    return *this;
}

Board::~Board()
{

}

unsigned int Board::count(const Player & player) const
{
    unsigned int ret = 0;

    if (_arr) {
        for (unsigned int i = 0; i < _size; ++i)
            if (_arr[i].color == player.getColor())
                ++ret;
    }
    return ret;
}

void Board::clear()
{
    for (unsigned int i = 0; i < _size; ++i) {
        _arr[i].color = 0;
        _arr[i].player = 0;
    }
}

std::ostream & operator<<(std::ostream & os, const Board & board)
{
    for (unsigned int i = 0; i < board.size2() + 2; ++i)
        os << ((!i || i == board.size2() + 1) ? '+' : '-');
    os << std::endl;

    for (unsigned int y = 0; y < board.size2(); ++y) {
        os << '|';
        for (unsigned int x = 0; x < board.size1(); ++x) {
            if (board[x][y].player)
                os << static_cast<char>(('A' - 1 + board[x][y].player));
            else if (board[x][y].color)
                os << static_cast<char>(('a' - 1 + board[x][y].color));
            else
                os << ' ';
        }
        os << '|' << std::endl;
    }

    for (unsigned int i = 0; i < board.size2() + 2; ++i)
        os << ((!i || i == board.size2() + 1) ? '+' : '-');
    os << std::endl;
    return os;
}
