#ifndef ARR2_HPP_
# define ARR2_HPP_

template <typename Type>
class Arr2
{
public:
    Arr2()
        : _arr(0)
        , _size(0)
        , _size1(0)
        , _size2(0)
    {

    }

    Arr2(unsigned int size1, unsigned int size2, Type value = Type())
        : _arr(0)
        , _size(size1 * size2)
        , _size1(size1)
        , _size2(size2)
    {
        _arr = new Type[_size];
        for (unsigned int i = 0; i < _size; ++i)
            _arr[i] = value;
    }

    Arr2(const Arr2 & o)
        : _arr(0)
        , _size(o._size)
        , _size1(o._size1)
        , _size2(o._size2)
    {
        _arr = new Type[_size];
        for (unsigned int i = 0; i < _size; ++i)
            _arr[i] = o._arr[i];
    }

    Arr2 & operator=(const Arr2 & o)
    {
        if (&o == this)
            return *this;

        _size = o._size;
        _size1 = o._size1;
        _size2 = o._size2;

        if (_arr)
            delete[] _arr;
        _arr = new Type[_size];
        for (unsigned int i = 0; i < _size; ++i)
            _arr[i] = o._arr[i];
        return *this;
    }

    virtual ~Arr2()
    {
        if (_arr)
            delete[] _arr;
    }

    Type * operator[](unsigned int index)
    {
        return &(_arr[index * _size2]);
    }

    const Type * operator[](unsigned int index) const
    {
        return &(_arr[index * _size2]);
    }

    void add1(unsigned int nb = 1, Type value = Type())
    {
        if (_arr) {
            Type * tmp = new Type[_size];

            for (unsigned int i = 0; i < _size; ++i)
                tmp[i] = _arr[i];

            delete[] _arr;
            _arr = new Type[(_size1 + nb) * _size2];

            for (unsigned int j = 0; j < _size2; ++j) {
                for (unsigned int i = 0; i < _size1; ++i)
                    _arr[(i * _size2) + j] = tmp[(i * _size2) + j];
                for (unsigned int i = _size1; i < _size1 + nb; ++i)
                    _arr[(i * _size2) + j] = value;
            }

            delete[] tmp;
        } else {
            _arr = new Type[nb];
            for (unsigned int i = 0; i < nb; ++i)
                _arr[i] = value;
            ++_size2;
        }

        _size1 += nb;
        _size = _size1 * _size2;
    }

    void add2(unsigned int nb = 1, Type value = Type())
    {
        if (_arr) {
            Type * tmp = new Type[_size];

            for (unsigned int i = 0; i < _size; ++i)
                tmp[i] = _arr[i];

            delete[] _arr;
            _arr = new Type[_size1 * (_size2 + nb)];

            for (unsigned int i = 0; i < _size1; ++i) {
                for (unsigned int j = 0; j < _size2; ++j)
                    _arr[(i * (_size2 + nb)) + j] = tmp[(i * _size2) + j];
                for (unsigned int j = _size2; j < _size2 + nb; ++j)
                    _arr[(i * (_size2 + nb)) + j] = value;
            }

            delete[] tmp;
        } else {
            _arr = new Type[nb];
            for (unsigned int i = 0; i < nb; ++i)
                _arr[i] = value;
            ++_size1;
        }

        _size2 += nb;
        _size = _size1 * _size2;
    }

    void sub1(unsigned int nb = 1)
    {
        if (_arr) {
            if (nb < _size1) {
                _size1 -= nb;
                _size = _size1 * _size2;

                Type * tmp = new Type[_size];

                for (unsigned int i = 0; i < _size; ++i)
                    tmp[i] = _arr[i];

                delete[] _arr;
                _arr = tmp;
            } else {
                delete[] _arr;
                _arr = 0;
                _size = 0;
                _size1 = 0;
                _size2 = 0;
            }
        }
    }

    void sub2(unsigned int nb = 1)
    {
        if (_arr) {
            if (nb < _size2) {
                Type * tmp = new Type[_size1 * (_size2 - nb)];

                for (unsigned int i = 0; i < _size1; ++i)
                    for (unsigned int j = 0; j < _size2 - nb; ++j)
                        tmp[(i * (_size2 - nb)) + j] = _arr[(i * _size2) + j];

                _size2 -= nb;
                _size = _size1 * _size2;
                delete[] _arr;
                _arr = tmp;
            } else {
                delete[] _arr;
                _arr = 0;
                _size = 0;
                _size1 = 0;
                _size2 = 0;
            }
        }
    }

    void setSize1(unsigned int size, Type value = Type())
    {
        if (size > _size1)
            add1(size - _size1, value);
        else
            sub1(_size1 - size);
    }

    void setSize2(unsigned int size, Type value = Type())
    {
        if (size > _size2)
            add2(size - _size2, value);
        else
            sub2(_size2 - size);
    }

    unsigned int size1() const
    {
        return _size1;
    }

    unsigned int size2() const
    {
        return _size2;
    }

    unsigned int size() const
    {
        return _size;
    }

    Type * data()
    {
        return _arr;
    }

protected:
    Type *       _arr; // Arr2[x][y] == _arr[(x * _size2) + y]
    unsigned int _size;
    unsigned int _size1;
    unsigned int _size2;
};

#endif // !ARR2_HPP_
