#include "SFMLGraphicRender.hpp"

SFMLGraphicRender::SFMLGraphicRender(const NeuronNetwork & network, unsigned int width, unsigned int height)
    : AGraphicRender(network, width, height)
{

}

SFMLGraphicRender::~SFMLGraphicRender()
{

}

bool SFMLGraphicRender::genImage(void * img)
{
    sf::Vector2u size(_width, _height);
    const std::vector<NeuronLayer> & layers = _network.getNeuronLayers();

    if (!img)
        return false;
    _render = static_cast<sf::RenderTexture *>(img);
    if (_render->getSize() != size)
        updateSize();
    _render->clear(sf::Color(0x00, 0x00, 0x00, 0x40));

    for (unsigned int i = 0; i < layers.size(); ++i) {
        const std::list<ANeuron *> & neurons = layers[i].getNeurons();
        unsigned int nb = 0;

        for (auto it = neurons.begin(); it != neurons.end(); ++it) {
            const ANeuron::SaveData & data = (*it)->getData();
            sf::RectangleShape rectangle(sf::Vector2f(_neuronSize, _neuronSize));

            // Draw Neurons
            rectangle.setFillColor(sf::Color::White);
            rectangle.setPosition(sf::Vector2f((i * _layerW) + (_layerW / 2) - (_neuronSize / 2), (nb * _layerH[i]) + (_layerH[i] / 2) - (_neuronSize / 2)));
            _render->draw(rectangle);

            // Draw Links
            sf::Vertex line[2];

            line[0].position = sf::Vector2f(rectangle.getPosition().x + _neuronSize, rectangle.getPosition().y + (_neuronSize / 2));
            if (i + 1 < layers.size()) {
                unsigned int nbl = 0;

                for (auto itl = data.link.begin(); itl != data.link.end(); ++itl) {
                    if (itl->valueSend) {
                        line[0].color    = itl->valueSend > 0 ? sf::Color::Green : sf::Color::Red;
                        line[1].position = sf::Vector2f(((i + 1) * _layerW) + (_layerW / 2) - (_neuronSize / 2), (nbl * _layerH[i + 1]) + (_layerH[i + 1] / 2));
                        line[1].color    = line[0].color;

                        _render->draw(line, 2, sf::LineStrip);
                    }
                    ++nbl;
                }
            } else if ((*it)->getValue() > 0) {
                line[0].position    = sf::Vector2f(rectangle.getPosition().x + _neuronSize, rectangle.getPosition().y + (_neuronSize / 2));
                line[0].color       = sf::Color::Green;
                line[1].position    = line[0].position;
                line[1].position.x += (_layerW - _neuronSize) / 4;
                line[1].color       = line[0].color;
                _render->draw(line, 2, sf::LineStrip);
            }
            ++nb;
        }
    }
    return true;
}
