#ifndef SFML_GRAPHICRENDER_HPP_
# define SFML_GRAPHICRENDER_HPP_

# include <SFML/Graphics.hpp>
# include <SFML/Window.hpp>
# include <SFML/System.hpp>

# include "LibNeuronNetwork.hpp"

class SFMLGraphicRender : public AGraphicRender
{
public:
    explicit SFMLGraphicRender(const NeuronNetwork & network, unsigned int width = 100, unsigned int height = 100);
    SFMLGraphicRender(const SFMLGraphicRender & o) = delete;
    SFMLGraphicRender & operator=(const SFMLGraphicRender & o) = delete;
    virtual ~SFMLGraphicRender();

    virtual bool genImage(void * img) override;

protected:
    sf::RenderTexture * _render;
};

#endif // !SFML_GRAPHICRENDER_HPP_
