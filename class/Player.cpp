#include "Player.hpp"

std::vector<Player> Player::newPlayers(unsigned int nb)
{
    std::vector<Player> players;

    for (unsigned int i = 0; i < nb; ++i)
        players.push_back(Player(BoardValue(i + 1, i + 1)));
    return players;
}

Player::Player(const BoardValue & value)
    : _value(value)
    , _posX(0)
    , _posY(0)
{

}

Player::Player(const Player & o)
    : _value(o._value)
{

}

Player & Player::operator=(const Player & o)
{
    if (this == &o)
        return *this;
    _value = o._value;
    return *this;
}

Player::~Player()
{

}

unsigned int Player::getColor() const
{
    return _value.color;
}

unsigned int Player::getID() const
{
    return _value.player;
}

unsigned int Player::getPosX() const
{
    return _posX;
}

unsigned int Player::getPosY() const
{
    return _posY;
}

void Player::setPosX(unsigned int posX)
{
    _posX = posX;
}

void Player::setPosY(unsigned int posY)
{
    _posY = posY;
}

void Player::setPos(unsigned int posX, unsigned int posY)
{
    _posX = posX;
    _posY = posY;
}
