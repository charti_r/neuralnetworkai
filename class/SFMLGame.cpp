#include "SFMLGame.hpp"

SFMLGame::SFMLGame(sf::RenderWindow & win, unsigned int nbPlayer)
    : Game(nbPlayer)
    , _renders(_nbPlayer, 0)
    , _wins(_nbPlayer, 0)
    , _win(win)
    , _winIsFocused(false)
{
    std::srand(std::time(0));
}

SFMLGame::~SFMLGame()
{
    for (unsigned int i = 0; i < _nbPlayer; ++i) {
        if (_renders[i])
            delete _renders[i];
        if (_wins[i])
            delete _wins[i];
    }
}

bool SFMLGame::onInit()
{
    _winIsFocused = true;

    if (_win.isOpen())
        for (unsigned int i = 0; i < _nbPlayer; ++i) {
            if (_renders[i])
                _renders[i]->setNetwork(_networks[i]);
            else
                _renders[i] = new SFMLGraphicRender(_networks[i], _win.getSize().x / (_nbPlayer > 1 ? _nbPlayer * 1.5f : 2), _win.getSize().y / (_nbPlayer > 1 ? _nbPlayer * 1.5f : 2));

            if (!_wins[i]) {
                _wins[i] = new sf::RenderTexture();
                if (_wins[i]->create(_renders[i]->getWidth(), _renders[i]->getHeight()) == false)
                    return false;
            }
        }
    return true;
}

void SFMLGame::handleEvent()
{
    if (_win.isOpen() == false)
        return;

    sf::Event event;
    while (_win.pollEvent(event)) {
        switch (event.type) {
            case sf::Event::Closed:      close();                                     break;
            case sf::Event::LostFocus:   _winIsFocused = false;                       break;
            case sf::Event::GainedFocus: _winIsFocused = true;                        break;
            case sf::Event::Resized:     resize(event.size.width, event.size.height); break;
            case sf::Event::KeyPressed:  handleKey(event.key.code);                   break;
            default:                                                                  break;
        }
    }
}

void SFMLGame::displayBoard()
{
    static const unsigned int spacing = 20;

    static sf::Color * colorArr = 0;
    static sf::Font  * font     = 0;
    static sf::Text    turnText;
    static sf::Text    speedText;
    static sf::Text    scoreText[2];

    if (_winIsFocused == false)
        return;

    if (!colorArr) {
        colorArr = new sf::Color[_nbPlayer + 1];

        for (unsigned int i = 0; i < (_nbPlayer + 1); ++i)
            colorArr[i] = sf::Color((std::rand() % 0x80) + 0x40, (std::rand() % 0x80) + 0x40, (std::rand() % 0x80) + 0x40);
    }

    if (!font) {
        font = new sf::Font;

        if (font->loadFromFile("resources/font/KeepCalm.ttf") || font->loadFromFile("resources/font/coolvetica.ttf")) {
            turnText.setFont(*font);
            turnText.setCharacterSize(50);
            turnText.setOutlineThickness(2);
            turnText.setFillColor(sf::Color(0xFF, 0xFF, 0xFF, 0xC0));
            turnText.setOutlineColor(sf::Color(0x00, 0x00, 0x00, 0xC0));

            speedText.setFont(*font);
            speedText.setCharacterSize(50);
            speedText.setOutlineThickness(2);
            speedText.setFillColor(sf::Color(0xFF, 0xFF, 0xFF, 0xC0));
            speedText.setOutlineColor(sf::Color(0x00, 0x00, 0x00, 0xC0));

            for (unsigned int i = 0; i < _nbPlayer; ++i) {
                scoreText[i].setFont(*font);
                scoreText[i].setCharacterSize(50);
                scoreText[i].setOutlineThickness(2);
                scoreText[i].setFillColor(colorArr[i] + sf::Color(0x00, 0x00, 0x00, 0xC0));
                scoreText[i].setOutlineColor(sf::Color(0x00, 0x00, 0x00, 0xC0));
            }
        }
    }

    const sf::Vector2f drawnSize(_win.getSize().x / _board.size1(), _win.getSize().y / _board.size2());
    sf::RectangleShape drawn(drawnSize);

    _win.clear(colorArr[_nbPlayer]);

    // Draw Board
    for (unsigned int x = 0; x < _board.size1(); ++x)
        for (unsigned int y = 0; y < _board.size2(); ++y)
            if (_board[x][y].color) {
                drawn.setOutlineThickness(_board[x][y].player ? (-(drawnSize.x < drawnSize.y ? drawnSize.x : drawnSize.y) / 8.f) : 0.f);
                drawn.setPosition(sf::Vector2f(drawnSize.x * x, drawnSize.y * y));
                drawn.setFillColor(colorArr[_board[x][y].color - 1]);
                _win.draw(drawn);
            }

    // Draw NeuronNetworks
    for (unsigned int i = 0; i < _nbPlayer; ++i)
        if (_renders[i] && _wins[i] && _renders[i]->genImage(_wins[i])) {
            unsigned int prex;

            _wins[i]->display();
            sf::Sprite sprite(_wins[i]->getTexture());

            prex = i * (_win.getSize().x / (_nbPlayer > 1 ? _nbPlayer - 1 : 1));
            sprite.setPosition(prex - (sprite.getLocalBounds().width * prex / _win.getSize().x), spacing);
            _win.draw(sprite);
        }

    // Draw Texts
    if (turnText.getFont() && speedText.getFont()) {
        turnText.setString(std::to_string(_turn <= _nbTurn ? _turn : _nbTurn) + std::string("/") + std::to_string(_nbTurn));
        speedText.setString(std::string("x") + std::to_string(static_cast<int>(_speed)));

        turnText.setPosition((_win.getSize().x / 2) - turnText.getLocalBounds().width - spacing, spacing);
        speedText.setPosition((_win.getSize().x / 2) + spacing, spacing);

        _win.draw(turnText);
        _win.draw(speedText);

        for (unsigned int i = 0; i < _nbPlayer; ++i) {
            unsigned int prex;

            prex = i * (_win.getSize().x / (_nbPlayer > 1 ? _nbPlayer - 1 : 1));
            scoreText[i].setString(std::to_string(_score[i]));
            scoreText[i].setPosition(prex - (scoreText[i].getLocalBounds().width * prex / _win.getSize().x), _win.getSize().y - scoreText[i].getLocalBounds().height - spacing);
            _win.draw(scoreText[i]);
        }
    }

    _win.display();
}

void SFMLGame::close()
{
    _win.close();
    _winIsFocused = false;
    _speed = maxSpeed;

    for (unsigned int i = 0; i < _nbPlayer; ++i) {
        if (_renders[i]) {
            delete _renders[i];
            _renders[i] = 0;
        }

        if (_wins[i]) {
            delete _wins[i];
            _wins[i] = 0;
        }
    }
}

void SFMLGame::resize(const unsigned int width, const unsigned int height)
{
    static const unsigned int minSize = 500;

    if (width < minSize || height < minSize) {
        _win.setSize(sf::Vector2u(width < minSize ? minSize : width, height < minSize ? minSize : height));
        _win.setView(sf::View(sf::FloatRect(0, 0, _win.getSize().x, _win.getSize().y)));
    } else {
        _win.setView(sf::View(sf::FloatRect(0, 0, width, height)));
    }

    for (unsigned int i = 0; i < _nbPlayer; ++i) {
        _renders[i]->updateSize(_win.getSize().x / (_nbPlayer > 1 ? _nbPlayer * 1.5f : 2), _win.getSize().y / (_nbPlayer > 1 ? _nbPlayer * 1.5f : 2));
        _wins[i]->create(_renders[i]->getWidth(), _renders[i]->getHeight());
        _wins[i]->setView(sf::View(sf::FloatRect(0, 0, _wins[i]->getSize().x, _wins[i]->getSize().y)));
    }
}

void SFMLGame::handleKey(unsigned int keycode)
{
    bool stop = false;

    switch (keycode)
    {
        case sf::Keyboard::Escape: close();                           break;
        case sf::Keyboard::Space:  _speed = _speed ? 0.f : 1000000.f; break;

        case sf::Keyboard::Up:
            if (!_speed) {
                upSpeed(1.f);
            } else {
                for (unsigned int i = 1000000; i && !stop; i /= 10)
                    if (_speed >= i) {
                        upSpeed(i);
                        stop = true;
                    }
            }
            break;

        case sf::Keyboard::Down:
            for (unsigned int i = 1000000; i && !stop; i /= 10)
                if (_speed > i || i == 1) {
                    downSpeed(i);
                    stop = true;
                }
            break;

        case sf::Keyboard::Right:
            if (!_speed) {
                upSpeed(1.f);
            } else {
                for (unsigned int i = 1000000; i && !stop; i /= 10)
                    if (_speed >= i) {
                        upSpeed(i / 10 ? (i / 10) : i);
                        stop = true;
                    }
            }
            break;

        case sf::Keyboard::Left:
            for (unsigned int i = 1000000; i && !stop; i /= 10)
                if (_speed > i || i == 1) {
                    downSpeed(i >= 10 ? (i / 10) : i);
                    stop = true;
                }
            break;

        default: break;
    }
}
