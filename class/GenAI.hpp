#ifndef GENAI_H_
# define GENAI_H_

# include <iostream>
# include <fstream>
# include <vector>
# include <string>
# include <thread>
# include <mutex>
# include <functional>

# include <SFML/Window.hpp>
# include <SFML/Graphics.hpp>

# include "LibNeuronNetwork.hpp"

# include "Arg.hpp"
# include "GenNeuronNetwork.hpp"
# include "NetworkData.hpp"
# include "Prob.hpp"
# include "LoadingDisplay.hpp"
# include "Game.hpp"
# include "SFMLGame.hpp"

class GenAI
{
public:
    GenAI();
    GenAI(const GenAI & o) = delete;
    GenAI & operator=(const GenAI & o) = delete;
    virtual ~GenAI();

    void help() const;
    bool init(int ac, char ** av);
    int  run();

private:
    void battles(bool & ret, unsigned int gen, unsigned int iThread);
    void genAIs();
    void genAIs(unsigned int iThread);
    bool loadAIs();
    void loadAIs(bool & ret, unsigned int iThread);
    bool saveAIs() const;

    void sort();
    void crossover();
    void crossover(unsigned int iThread);

    void handleEvent();
    void close();
    void resize(unsigned int width, unsigned int height);
    void handleKey(unsigned int keycode);
    void loadingScreen(const std::string & str);
    void updateLoadingDisplay(unsigned int nb = 1);

private:
    GeneticAlgo                               _genetic;
    std::vector<GenNeuronNetwork::ParamLayer> _genParam;
    std::vector<NetworkData>                  _networkData;
    Prob                                      _prob;
    unsigned int                              _nbPlayer;
    unsigned int                              _population;
    unsigned int                              _nbGen;
    unsigned int                              _nbBattle;
    unsigned int                              _nbTurn;

    sf::RenderWindow _win;
    float            _speed;

    bool           _isHelp;
    bool           _isVerbose;
    unsigned int   _nbThread;
    std::mutex     _mutex;
    LoadingDisplay _loadingDisplay;
    std::string    _saveDir;
    std::string    _loadDir;
};

#endif // !GENAI_H_
