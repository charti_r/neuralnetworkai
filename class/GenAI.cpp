#include "GenAI.hpp"

static inline bool is_not_num(char c)
{
    return !isdigit(c);
}

static inline bool is_not_fnum(char c)
{
    return !(isdigit(c) || c == '.');
}

GenAI::GenAI()
{

}

GenAI::~GenAI()
{

}

void GenAI::help() const
{
    std::cout << "Usage: genAI (options)" << std::endl;
    std::cout << "\t-h: print this help" << std::endl;
    std::cout << "\t-v: verbose" << std::endl;
    std::cout << "\t-n: player number [unsigned int (2)]" << std::endl;
    std::cout << "\t-p: population [unsigned int (1000)]" << std::endl;
    std::cout << "\t-g: generation number [unsigned int (1000)]" << std::endl;
    std::cout << "\t-t: thread number [unsigned int (1)]" << std::endl;
    std::cout << "\t-c: crossover percentage [float (0.025f)]" << std::endl;
    std::cout << "\t-m: mutation percentage [float (0.02f)]" << std::endl;
    std::cout << "\t-l: load directory [string]" << std::endl;
    std::cout << "\t-s: save directory [string]" << std::endl;
    std::cout << "\t-G: graphic display" << std::endl;
    std::cout << "\t-S: graphic speed [unsigned int (" << Game::maxSpeed << ")]" << std::endl;
    std::cout << "\t-T: turn number [unsigned int (25)]" << std::endl;
    std::cout << "\t-W: window's width [unsigned int (1000)]" << std::endl;
    std::cout << "\t-H: window's height [unsigned int (1000)]" << std::endl;
    std::cout << std::endl;
}

bool GenAI::init(int ac, char ** av)
{
    Arg arg(ac, av);

    std::map<char, bool> paramb;
    std::map<char, unsigned int> paramu;
    std::map<char, float> paramf;
    std::map<char, std::string> params;

    paramb['h'] = false;           // help
    paramb['v'] = false;           // verbose
    paramb['G'] = false;           // graphic
    paramu['n'] = 2;               // nbPlayer
    paramu['p'] = 1000;            // population
    paramu['g'] = 1000;            // nbGen
    paramu['t'] = 1;               // nbThread
    paramu['T'] = 25;              // nbTurn
    paramu['W'] = 1000;            // witdh
    paramu['H'] = 1000;            // height
    paramf['c'] = 0.025f;          // crossPercent
    paramf['m'] = 0.02f;           // mutPercent
    paramf['S'] = Game::maxSpeed;  // speed
    params['l'] = std::string(""); // loadDir
    params['s'] = std::string(""); // saveDir

    for (unsigned int i = 1; i < arg.size(); ++i) {
        bool forward = false;

        if (arg[i][0] == '-') {
            for (unsigned int j = 1; j < arg[i].size(); ++j) {
                const std::string parambool("hvG");
                const std::string paramuint("npgtTWH");
                const std::string paramfloat("cmS");
                const std::string paramstring("sl");

                if (std::find(parambool.begin(), parambool.end(), arg[i][j]) != parambool.end()) {// bool
                    paramb[arg[i][j]] = true;
                } else if (i + 1 < arg.size()) {
                    if (std::find(paramuint.begin(), paramuint.end(), arg[i][j]) != paramuint.end()) { // unsigned int
                        if (std::find_if(arg[i + 1].begin(), arg[i + 1].end(), is_not_num) != arg[i + 1].end() || arg[i + 1][0] == '-') {
                            std::cerr << "param " << arg[i][j] << " must be an unsigned integer" << std::endl;
                            return false;
                        }
                        paramu[arg[i][j]] = std::stoll(arg[i + 1]);
                        forward = true;
                    } else if (std::find(paramfloat.begin(), paramfloat.end(), arg[i][j]) != paramfloat.end()) { // float
                        if (std::find_if(arg[i + 1].begin(), arg[i + 1].end(), is_not_fnum) != arg[i + 1].end()) {
                            std::cerr << "param " << arg[i][j] << " value is not a float number" << std::endl;
                            return false;
                        }
                        paramf[arg[i][j]] = std::stof(arg[i + 1]);
                        forward = true;
                    } else if (std::find(paramstring.begin(), paramstring.end(), arg[i][j]) != paramstring.end()) { // string
                        params[arg[i][j]] = arg[i + 1];
                        forward = true;
                    } else {
                        std::cerr << "param " << arg[i][j] << " not known and ignored" << std::endl;
                    }
                } else {
                    std::cerr << "param " << arg[i][j] << " missing value" << std::endl;
                }
            }
        } else {
            // TODO i?/h/o uint ?
        }
        if (forward)
            ++i;
    }

    _isHelp     = paramb['h'];
    _isVerbose  = paramb['v'];
    _nbPlayer   = paramu['n'] ? paramu['n'] : 1;
    _population = paramu['p'];
    _nbGen      = paramu['g'];
    _nbBattle   = _population / _nbPlayer;
    _nbThread   = paramu['t'] ? paramu['t'] : 1;
    _nbTurn     = paramu['T'] ? paramu['T'] : 1;
    _speed      = paramf['S'];
    _loadDir    = params['l'];
    _saveDir    = params['s'];

    if (_loadDir.size() && _loadDir[_loadDir.size() - 1] != '/')
        _loadDir += '/';
    if (_saveDir.size() && _saveDir[_saveDir.size() - 1] != '/')
        _saveDir += '/';

    _genetic.setCrossPercent(paramf['c']);
    _genetic.setMutPercent(paramf['m']);

    if (_isHelp)
        return true;

    if (_population > 29308) {
        std::cerr << "Population must be lower that 29308" << std::endl;
        return false;
    }
    if (_population % (_nbPlayer * 2)) {
        std::cerr << "Population % (nbPlayer x 2) must be equal to 0 (aka " << _population % (_nbPlayer * 2) << ")" << std::endl;
        return false;
    }
    if ((_nbPlayer == 1 ? _population / 2 : _population) % _nbThread) {
        if (_nbPlayer == 1)
            std::cerr << "(Population / 2) % nbThread must be equal to 0 (aka " << ((_population / 2) % _nbThread) << ")" << std::endl;
        else
            std::cerr << "Population % nbThread must be equal to 0 (aka " << (_population % _nbThread) << ")" << std::endl;
        return false;
    }

    _networkData.resize(_population);
    _prob.setSize(_population / 2);
    _genParam.push_back(GenNeuronNetwork::ParamLayer("input.simple", 200));
    _genParam.push_back(GenNeuronNetwork::ParamLayer("hidden.simple", 50));
    _genParam.push_back(GenNeuronNetwork::ParamLayer("hidden.simple", 16));
    _genParam.push_back(GenNeuronNetwork::ParamLayer("output.simple", 4));

    // if graphic
    if (paramb['G']) {
        _win.create(sf::VideoMode(paramu['W'], paramu['H']), arg[0]);
        resize(paramu['W'], paramu['H']); // minSize
    } else if (_win.isOpen()) {
        _win.close();
    }

    // loadFromFile or Generate AIs
    if (!_loadDir.size())
        genAIs();
    else if (loadAIs() == false)
        return false;
    return true;
}

int GenAI::run()
{
    if (_isHelp) {
        help();
        return 0;
    }

    for (unsigned int gen = 0; gen < _nbGen; ++gen) {
        bool ret = true;

        std::cout << "Generation: " << (gen + 1) << " / " << _nbGen << " - Population: " << _population << std::endl;
        _loadingDisplay.reset(_nbPlayer == 1 && gen ? _nbBattle / 2 : _nbBattle);
        if (_nbThread > 1) {
            std::vector<std::thread> threads;

            for (unsigned int i = _win.isOpen() ? 1 : 0; i < _nbThread; ++i)
                threads.push_back(std::thread(&GenAI::battles, this, std::ref(ret), gen, i));

            if (_win.isOpen())
                battles(ret, gen, 0);
            for (unsigned int i = 0; i < threads.size(); ++i)
                threads[i].join();
        } else {
            battles(ret, gen, 0);
        }
        if (ret == false)
            return 1;
        std::cout << "\r" << std::flush;

        sort();
        if (gen + 1 < _nbGen)
            crossover();
        std::cout << std::endl;
    }

    if (_saveDir.size() && saveAIs() == false)
        return 1;
    return 0;
}

void GenAI::battles(bool & ret, unsigned int gen, unsigned int iThread)
{
    const unsigned int size = _nbPlayer == 1 && gen ? _nbBattle / 2 : _nbBattle;
    const unsigned int beg  = ((size * iThread) / _nbThread) + (_nbPlayer == 1 && gen ? _nbBattle / 2 : 0);
    const unsigned int end  = beg + (size / _nbThread);
    const bool isGraphic    = _win.isOpen() && !iThread;
    Game * game             = 0;

    // Instantiate Game
    if (isGraphic) {
        game = new SFMLGame(_win, _nbPlayer);
    } else {
      game = new Game(_nbPlayer);
      game->setSpeed(Game::maxSpeed);
    }
    if (!game) {
      ret = false;
      return;
    }

    for (unsigned int battle = beg; battle < end; ++battle) {
        if (_isVerbose)
            std::cout << "Battle " << (battle + 1) << " / " << _nbBattle << std::endl;

        // Init game for the battle (construct NeuronNetworks here)
        if (isGraphic)
            _win.setTitle(std::string("Generation: ") + std::to_string(gen + 1) + std::string(" / ") + std::to_string(_nbGen) + std::string(" - Population: ") + std::to_string(_population) + std::string(" - Battle: ") + std::to_string(battle - beg + 1) + std::string(" / ") + std::to_string(end - beg));
        if (game->init(&_networkData[battle * _nbPlayer], &(_networkData[(battle + 1) * _nbPlayer])) == false) {
            ret = false;
            return;
        }

        // Run Game & save fitness
        if (isGraphic)
            game->setSpeed(_speed);
        std::vector<unsigned int> count(game->run(_nbTurn));
        for (unsigned int i = 0; i < count.size(); ++i)
            _networkData[(battle * count.size()) + i].fitness = count[i];
        if (isGraphic)
            _speed = game->getSpeed();
        updateLoadingDisplay();
    }
    delete game;
}

void GenAI::genAIs()
{
    std::cout << "generating " << _population << " DNAs..." << std::endl;
    _loadingDisplay.reset(_networkData.size());
    if (_nbThread > 1) {
        std::vector<std::thread> threads;

        for (unsigned int i = _win.isOpen() ? 1 : 0; i < _nbThread; ++i)
            threads.push_back(std::thread(static_cast<void (GenAI::*)(unsigned int)>(&GenAI::genAIs), this, i));

        if (_win.isOpen())
            genAIs(0);
        for (unsigned int i = 0; i < threads.size(); ++i)
            threads[i].join();
    } else {
        genAIs(0);
    }
    std::cout << "\r" << std::flush;
}

void GenAI::genAIs(unsigned int iThread)
{
    const unsigned int size = _networkData.size();
    const unsigned int beg  = (size * iThread) / _nbThread;
    const unsigned int end  = beg + (size / _nbThread);
    const bool isGraphic    = _win.isOpen() && !iThread;
    GenNeuronNetwork generator;

    for (unsigned int i = beg; i < end; ++i) {
        generator.gen(_genParam, 200, 20, 200);
        _networkData[i].data << generator;
        updateLoadingDisplay();
        if (isGraphic)
            loadingScreen("Generating");
    }
}

bool GenAI::loadAIs()
{
    bool ret = true;

    std::cout << "loading " << _population << " DNAs..." << std::endl;
    _loadingDisplay.reset(_networkData.size());
    if (_nbThread > 1) {
        std::vector<std::thread> threads;

        for (unsigned int i = _win.isOpen() ? 1 : 0; i < _nbThread; ++i)
            threads.push_back(std::thread(static_cast<void (GenAI::*)(bool &, unsigned int)>(&GenAI::loadAIs), this, std::ref(ret), i));

        if (_win.isOpen())
            loadAIs(ret, 0);
        for (unsigned int i = 0; i < threads.size(); ++i)
            threads[i].join();
    } else {
        loadAIs(ret, 0);
    }
    std::cout << "\r" << std::flush;
    return ret;
}

void GenAI::loadAIs(bool & ret, unsigned int iThread)
{
    const unsigned int size = _networkData.size();
    const unsigned int beg  = (size * iThread) / _nbThread;
    const unsigned int end  = beg + (size / _nbThread);
    const bool isGraphic    = _win.isOpen() && !iThread;
    GenNeuronNetwork generator;

    for (unsigned int i = beg; i < end; ++i) {
        std::string filename(_loadDir + std::to_string(i) + ".nnm");

        _mutex.lock(); // Save HDD
        if (generator.readFromFile(filename) == false) {
            std::cerr << "Cant load from file " << filename << std::endl;
            ret = false;
            _mutex.unlock();
            return;
        }
        _mutex.unlock();
        _networkData[i].data << generator;
        updateLoadingDisplay();
        if (isGraphic)
            loadingScreen("Loading");
    }
}

bool GenAI::saveAIs() const
{
    std::cout << "saving files..." << std::endl;
    for (unsigned int i = 0; i < _networkData.size(); ++i) {
        std::ofstream of;

        // id.nnm
        of.open(std::string(_saveDir + std::to_string(i) + ".nnm").c_str());
        if (of.is_open() == false) {
            std::cerr << "Cant save files" << std::endl;
            return false;
        }
        of << _networkData[i].data;
        of.close();
    }
    return true;
}

void GenAI::sort()
{
    const unsigned int midSize = _networkData.size() / 2;
    unsigned int tmp;

    std::sort(_networkData.begin(), _networkData.end(), &NetworkData::compareNetworkData);
    tmp = 0;
    if (_isVerbose)
        std::cout << "Survivors" << std::endl;
    for (unsigned int i = 0; i < midSize; ++i) {
        if (i ? _networkData[i].fitness == _networkData[i - 1].fitness : true) {
            ++tmp;
            if (i == midSize - 1)
                std::cout << _networkData[i].fitness << " x" << tmp << std::endl;
        } else {
            std::cout << _networkData[i - 1].fitness << " x" << tmp << std::endl;
            tmp = 1;
            if (i == midSize - 1)
                std::cout << _networkData[i].fitness << " x1" << std::endl;
        }
    }

    if (_isVerbose) {
        tmp = 0;
        std::cout << "Eliminated" << std::endl;
        for (unsigned int i = midSize; i < _networkData.size(); ++i) {
            if (i > midSize ? _networkData[i].fitness == _networkData[i - 1].fitness : true) {
                ++tmp;
                if (i == _networkData.size() - 1)
                    std::cout << _networkData[i].fitness << " x" << tmp << std::endl;
            } else {
                std::cout << _networkData[i - 1].fitness << " x" << tmp << std::endl;
                tmp = 1;
                if (i == _networkData.size() - 1)
                    std::cout << _networkData[i].fitness << " x1" << std::endl;
            }
        }
    }
}

void GenAI::crossover()
{
    // Regenarate the population from survivors <3
    std::cout << "crossover..." << std::endl;
    _loadingDisplay.reset(_networkData.size() / 2);
    if (_nbThread > 1) {
        std::vector<std::thread> threads;

        for (unsigned int i = _win.isOpen() ? 1 : 0; i < _nbThread; ++i)
            threads.push_back(std::thread(static_cast<void (GenAI::*)(unsigned int)>(&GenAI::crossover), this, i));

        if (_win.isOpen())
            crossover(0);
        for (unsigned int i = 0; i < threads.size(); ++i)
            threads[i].join();
    } else {
        crossover(0);
    }
    std::cout << "\r---- ---- ---- ----" << std::flush;
}

void GenAI::crossover(unsigned int iThread)
{
    const unsigned int midSize = _networkData.size() / 2;
    const unsigned int beg     = (midSize * iThread) / _nbThread;
    const unsigned int end     = beg + (midSize / _nbThread);
    const bool isGraphic       = _win.isOpen() && !iThread;

    for (unsigned int i = beg; i < end; i += 2) {
        const std::vector<bool> data1 = GenNeuronNetwork::getDNA(_networkData[_prob.getIndex()].data);
        const std::vector<bool> data2 = GenNeuronNetwork::getDNA(_networkData[_prob.getIndex()].data);

        // Mixing !
        std::vector<bool> res1, res2;
        _genetic.crossover(data1, data2, res1, res2);
        GenNeuronNetwork::setDNA(_genParam, res1, _networkData[midSize + i].data);
        GenNeuronNetwork::setDNA(_genParam, res2, _networkData[midSize + i + 1].data);

        updateLoadingDisplay(2);
        if (isGraphic)
            loadingScreen("Crossover");
    }
}

void GenAI::handleEvent()
{
    sf::Event event;

    while (_win.pollEvent(event)) {
        if (event.type == sf::Event::Closed)
            close();
        else if (event.type == sf::Event::Resized)
            resize(event.size.width, event.size.height);
        else if (event.type == sf::Event::KeyPressed)
            handleKey(event.key.code);
    }
}

void GenAI::close()
{
    _win.close();
}

void GenAI::resize(unsigned int width, unsigned int height)
{
    static const unsigned int minSize = 500;

    if (width < minSize || height < minSize) {
        _win.setSize(sf::Vector2u(width < minSize ? minSize : width, height < minSize ? minSize : height));
        _win.setView(sf::View(sf::FloatRect(0, 0, _win.getSize().x, _win.getSize().y)));
    } else {
        _win.setView(sf::View(sf::FloatRect(0, 0, width, height)));
    }
}

void GenAI::handleKey(unsigned int keycode)
{
    switch (keycode) {
        case sf::Keyboard::Escape:
            close();
            break;
        default:
            break;
    }
}

void GenAI::loadingScreen(const std::string & str)
{
    static sf::Font font;
    sf::Text text;
    sf::RectangleShape drawn;

    _win.clear();
    handleEvent();

    drawn.setSize(sf::Vector2f(_win.getSize().x * 0.90f, _win.getSize().y * 0.10f));
    drawn.setPosition(sf::Vector2f(_win.getSize().x / 2 - drawn.getSize().x / 2, _win.getSize().y / 2 - drawn.getSize().y / 2));
    drawn.setOutlineThickness(10);
    drawn.setOutlineColor(sf::Color::White);
    drawn.setFillColor(sf::Color::Black);
    _win.draw(drawn);

    drawn.setSize(sf::Vector2f(drawn.getSize().x * _loadingDisplay.percent(1), drawn.getSize().y));
    drawn.setOutlineThickness(0);
    drawn.setFillColor(sf::Color::White);
    _win.draw(drawn);

    font.loadFromFile("resources/font/KeepCalm.ttf");
    text.setFont(font);
    text.setCharacterSize(100);
    text.setString(str);
    text.setOutlineThickness(4);
    text.setPosition((_win.getSize().x / 2) - (text.getLocalBounds().width / 2), drawn.getPosition().y - (2 * text.getLocalBounds().height));
    text.setOutlineColor(sf::Color::White);
    text.setFillColor(sf::Color::Black);
    _win.draw(text);

    _win.display();
}

void GenAI::updateLoadingDisplay(unsigned int nb)
{
    _mutex.lock();
    _loadingDisplay.add(nb);
    std::cout << "\r" << _loadingDisplay << std::flush;
    _mutex.unlock();
}
