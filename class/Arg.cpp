#include "Arg.hpp"

Arg::Arg(int ac, char ** av)
    : _val(ac)
{
    for (unsigned int i = 0; i < _val.size(); ++i)
        _val[i] = std::string(av[i]);
}

Arg::Arg(const Arg & o)
    : _val(o._val)
{

}

Arg & Arg::operator=(const Arg & o)
{
    if (this == &o)
        return *this;
    _val = o._val;
    return *this;
}

Arg::~Arg()
{

}

unsigned int Arg::size() const
{
    return _val.size();
}

const std::string & Arg::operator[](unsigned int index) const
{
    return _val[index];
}
