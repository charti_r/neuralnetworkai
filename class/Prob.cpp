#include "Prob.hpp"

Prob::Prob()
{
    std::srand(time(0));
}

Prob::Prob(unsigned int size)
{
    std::srand(time(0));
    setSize(size);
}

Prob::Prob(const Prob & o)
    : _arr(o._arr)
{

}

Prob & Prob::operator=(const Prob & o)
{
    if (this == &o)
        return *this;
    _arr = o._arr;
    return *this;
}

Prob::~Prob()
{

}

void Prob::setSize(unsigned int size)
{
    _arr.resize((size % 2) ? (((size + 1) / 2) * size) : ((size / 2) * (size + 1)));

    unsigned int pos = 0;
    for (unsigned int i = 0; i < size; ++i)
        for (unsigned int j = 0; j < (size - i); ++j)
            _arr[pos++] = i;
}

unsigned int Prob::getIndex() const
{
    return _arr[std::rand() % _arr.size()];
}
