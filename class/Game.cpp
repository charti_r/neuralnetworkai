#include "Game.hpp"

Game::Game(unsigned int nbPlayer)
    : _nbPlayer(nbPlayer ? nbPlayer : 1)
    , _board(10, 10)
    , _score(_nbPlayer, 0)
    , _players(Player::newPlayers(_nbPlayer))
    , _networks(_nbPlayer)
    , _neuronInputs(_nbPlayer)
    , _turn(0)
    , _nbTurn(0)
    , _clock()
    , _speed(1.f)
    , _action()
{

}

Game::~Game()
{

}

bool Game::init(const NetworkData * first, const NetworkData * last)
{
    if (last - first != _nbPlayer)
        return false;

    _board.clear();
    for (unsigned int i = 0; i < _nbPlayer; ++i) {
        //Init player
        _players[i].setPos((_board.size1() / (_nbPlayer * 2)) + (i * _board.size1() / _nbPlayer), _board.size2() / 2);
        _board[_players[i].getPosX()][_players[i].getPosY()].player = _players[i].getID();
        _board[_players[i].getPosX()][_players[i].getPosY()].color = _players[i].getColor();
        _score[i] = 1;

        // Init NeuronNetwork
        _networks[i] << first[i].data;
        std::vector<NeuronLayer> & layers = _networks[i].getNeuronLayers();

        //Init input layer
        if (layers[0].size() != _board.size() * 2) {
            std::cerr << "Inputs number (" << layers[0].size() << ") is not equal to the board size x 2 (" << (_board.size() * 2) << ")" << std::endl;
            return false;
        }
        _neuronInputs[i] = layers[0].getNeurons();

        //Init output layer
        if (layers[layers.size() - 1].size() != 4) {
            std::cerr << "Outputs number (" << layers[layers.size() - 1].size() << ") is not equal to the number of actions (4)" << std::endl;
            return false;
        }
        std::list<ANeuron *> & neuronOutputs = layers[layers.size() - 1].getNeurons();
        void (Game::*ptrArr[])(Player &) = {&Game::moveW, &Game::moveA, &Game::moveS, &Game::moveD};
        unsigned int iAction = 0;
        for (auto it = neuronOutputs.begin(); it != neuronOutputs.end(); ++it)
            _action[(*it)->getId()] = ptrArr[iAction++];
    }
    return onInit();
}

std::vector<unsigned int> Game::run(unsigned int nbTurn)
{
    std::vector<unsigned int> count(_nbPlayer, 0);

    // Game's loop
    _clock.restart();
    _nbTurn = nbTurn;
    for (_turn = 0; _turn < _nbTurn + 1; ++_turn) {
        handleEvent();
        while (_speed ? (_clock.getElapsedTime().asMicroseconds() < (maxSpeed / _speed)) : true) {
            handleEvent();
            displayBoard();
        }

        _clock.restart();
        displayBoard();
        if (_turn < _nbTurn)
            playPlayers();
    }

    // Final display & count
    for (unsigned int i = 0; i < _nbPlayer; ++i)
        count[i] = _board.count(_players[i]);
    return count;
}

unsigned int Game::getNbPlayer() const
{
    return _nbPlayer;
}

void Game::setSpeed(float speed)
{
    _speed = speed < 0.f ? 0.f : speed > maxSpeed ? maxSpeed : speed;
}

void Game::upSpeed(float upSpeed)
{
    _speed += upSpeed;
    if (_speed > maxSpeed)
        _speed = maxSpeed;
}

void Game::downSpeed(float downSpeed)
{
    _speed -= downSpeed;
    if (_speed < 0.f)
        _speed = 0.f;
}

float Game::getSpeed() const
{
    return _speed;
}

bool Game::onInit()
{
    return true;
}

void Game::handleEvent()
{

}

void Game::displayBoard()
{

}

void Game::playPlayers()
{
    bool isMoving;

    isMoving = false;
    for (unsigned int i = 0; i < _nbPlayer; ++i) {
        // move{X,Y} help to move relativly player in center of map
        const int moveX = (_board.size1() / 2) - _players[i].getPosX();
        const int moveY = (_board.size2() / 2) - _players[i].getPosY();

        _networks[i].reset();
        auto it = _neuronInputs[i].begin();
        for (unsigned int j = 0; j < 2; ++j)
            for (unsigned int x = 0; x < _board.size1(); ++x)
                for (unsigned int y = 0; y < _board.size2(); ++y) {
                    int posX = x + moveX;
                    int posY = y + moveY;

                    posX = posX < 0 ? posX + _board.size1() : posX % _board.size1();
                    posY = posY < 0 ? posY + _board.size2() : posY % _board.size2();
                    if (j)
                        (*it)->sendValue(_board[posX][posY].player - 1 == i ? 0 : _board[posX][posY].player);
                    else
                        (*it)->sendValue(_board[posX][posY].color);
                    ++it;
                }
        const ANeuronOutput * output = _networks[i].run();

        // Move Player due to network run
        if (output) {
            (this->*_action[output->getId()])(_players[i]);
            isMoving = true;
        }
    }

    if (!isMoving)
        _turn = _nbTurn;
}

void Game::moveW(Player & p)
{
    unsigned int posy = p.getPosY() ? p.getPosY() - 1 : _board.size2() - 1;

    if (_board[p.getPosX()][posy].player == 0) {
        _board[p.getPosX()][p.getPosY()].player = 0;
        _board[p.getPosX()][posy].player = p.getID();

        if (_board[p.getPosX()][posy].color) {
            if (_board[p.getPosX()][posy].color != p.getColor()) {
                _score[p.getID() - 1] += 1;
                _score[_board[p.getPosX()][posy].color - 1] -= 1;
            }
        } else {
            _score[p.getID() - 1] += 1;
        }

        _board[p.getPosX()][posy].color = p.getColor();
        p.setPosY(posy);
    }
}

void Game::moveA(Player & p)
{
    unsigned int posx = p.getPosX() ? p.getPosX() - 1 : _board.size1() - 1;

    if (_board[posx][p.getPosY()].player == 0) {
        _board[p.getPosX()][p.getPosY()].player = 0;
        _board[posx][p.getPosY()].player = p.getID();

        if (_board[posx][p.getPosY()].color) {
            if (_board[posx][p.getPosY()].color != p.getColor()) {
                _score[p.getID() - 1] += 1;
                _score[_board[posx][p.getPosY()].color - 1] -= 1;
            }
        } else {
            _score[p.getID() - 1] += 1;
        }

        _board[posx][p.getPosY()].color = p.getColor();
        p.setPosX(posx);
    }
}

void Game::moveS(Player & p)
{
    unsigned int posy = p.getPosY() + 1 < _board.size2() ? p.getPosY() + 1 : 0;

    if (_board[p.getPosX()][posy].player == 0) {
        _board[p.getPosX()][p.getPosY()].player = 0;
        _board[p.getPosX()][posy].player = p.getID();

        if (_board[p.getPosX()][posy].color) {
            if (_board[p.getPosX()][posy].color != p.getColor()) {
                _score[p.getID() - 1] += 1;
                _score[_board[p.getPosX()][posy].color - 1] -= 1;
            }
        } else {
            _score[p.getID() - 1] += 1;
        }

        _board[p.getPosX()][posy].color = p.getColor();
        p.setPosY(posy);
    }
}

void Game::moveD(Player & p)
{
    unsigned int posx = p.getPosX() + 1 < _board.size1() ? p.getPosX() + 1 : 0;

    if (_board[posx][p.getPosY()].player == 0) {
        _board[p.getPosX()][p.getPosY()].player = 0;
        _board[posx][p.getPosY()].player = p.getID();

        if (_board[posx][p.getPosY()].color) {
            if (_board[posx][p.getPosY()].color != p.getColor()) {
                _score[p.getID() - 1] += 1;
                _score[_board[posx][p.getPosY()].color - 1] -= 1;
            }
        } else {
            _score[p.getID() - 1] += 1;
        }

        _board[posx][p.getPosY()].color = p.getColor();
        p.setPosX(posx);
    }
}
