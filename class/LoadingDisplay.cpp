#include "LoadingDisplay.hpp"

LoadingDisplay::LoadingDisplay(unsigned int size)
    : _count(0)
    , _size(size)
{

}

LoadingDisplay::LoadingDisplay(const LoadingDisplay & o)
    : _count(o._count)
    , _size(o._size)
{

}

LoadingDisplay & LoadingDisplay::operator=(const LoadingDisplay & o)
{
    if (this == &o)
        return *this;
    _count = o._count;
    _size = o._size;
    return *this;
}

LoadingDisplay::~LoadingDisplay()
{

}

void LoadingDisplay::add(unsigned int nb)
{
    _count += nb;
}

float LoadingDisplay::percent(unsigned int on) const
{
    return static_cast<float>(_count * on) / _size;
}

void LoadingDisplay::reset()
{
    _count = 0;
}

void LoadingDisplay::reset(unsigned int size)
{
    _count = 0;
    _size = size;
}

unsigned int LoadingDisplay::count() const
{
    return _count;
}

void LoadingDisplay::setCount(unsigned int count)
{
    _count = count;
}

unsigned int LoadingDisplay::size() const
{
    return _size;
}

void LoadingDisplay::setSize(unsigned int size)
{
    _size = size;
}

std::ostream & operator<<(std::ostream & os, const LoadingDisplay & ld)
{
    return os << (ld.count() * 100 / ld.size()) << "%";
}
