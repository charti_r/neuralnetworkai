#ifndef BOARD_VALUE_HPP_
# define BOARD_VALUE_HPP_

struct BoardValue
{
public:
    BoardValue();
    explicit BoardValue(unsigned int color_, unsigned int player_);
    BoardValue(const BoardValue & o);
    BoardValue & operator=(const BoardValue & o);
    virtual ~BoardValue();

    operator int() const;

public:
    unsigned int color;
    unsigned int player;
};

#endif // !BOARD_VALUE_HPP_
