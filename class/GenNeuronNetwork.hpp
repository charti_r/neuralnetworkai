#ifndef GENNEURONNETWORK_HPP_
# define GENNEURONNETWORK_HPP_

# include <vector>
# include <string>
# include <cstdlib>
# include <ctime>

# include "LibNeuronNetwork.hpp"

class GenNeuronNetwork : public NeuronNetwork
{
public:
    struct ParamLayer
    {
        explicit ParamLayer(const std::string & type_, unsigned int nb_);
        ParamLayer(const ParamLayer & o);
        ParamLayer & operator=(const ParamLayer & o);
        virtual ~ParamLayer();

        std::string  type;
        unsigned int nb;
    };

public:
    GenNeuronNetwork();
    GenNeuronNetwork(const GenNeuronNetwork & o);
    GenNeuronNetwork & operator=(const GenNeuronNetwork & o);
    virtual ~GenNeuronNetwork();

    bool gen(const std::vector<ParamLayer> & params, unsigned int weightLimit = 200, unsigned int thresholdLimit = 50, unsigned int linkWeighLimit = 200);

    static std::vector<bool> getDNA(const std::string & data);
    static bool              setDNA(const std::vector<ParamLayer> & params, const std::vector<bool> & dna, std::string & data);

    std::vector<bool> getDNA() const;
    bool              setDNA(const std::vector<ParamLayer> & params, const std::vector<bool> & dna);
};

#endif // !GENNEURONNETWORK_HPP_
