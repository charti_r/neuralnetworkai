#ifndef PROB_HPP_
# define PROB_HPP_

# include <vector>
# include <cstdlib>
# include <ctime>

class Prob
{
public:
    Prob();
    explicit Prob(unsigned int size);
    Prob(const Prob & o);
    Prob & operator=(const Prob & o);
    virtual ~Prob();

    void setSize(unsigned int size);
    unsigned int getIndex() const;

protected:
    std::vector<unsigned int> _arr;
};

#endif // !PROB_HPP_
