#include "BoardValue.hpp"

BoardValue::BoardValue()
    : color(0)
    , player(0)
{

}

BoardValue::BoardValue(unsigned int color_, unsigned int player_)
    : color(color_)
    , player(player_)
{

}

BoardValue::BoardValue(const BoardValue & o)
    : color(o.color)
    , player(o.player)
{

}

BoardValue & BoardValue::operator=(const BoardValue & o)
{
    if (this == &o)
        return *this;
    color = o.color;
    player = o.player;
    return *this;
}

BoardValue::~BoardValue()
{

}

BoardValue::operator int() const
{
    return color + (player << 2);
}
