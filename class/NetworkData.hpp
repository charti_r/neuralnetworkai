#ifndef NETWORK_DATA_HPP_
# define NETWORK_DATA_HPP_

struct NetworkData
{
    NetworkData(const std::string & str = "", unsigned int nb = 0)
        : data(str)
        , fitness(nb)
    {

    }

    static bool compareNetworkData(const NetworkData & a, const NetworkData & b)
    {
        return a.fitness > b.fitness;
    }

    std::string  data;
    unsigned int fitness;
};

#endif // !NETWORK_DATA_HPP_
