#ifndef PLAYER_HPP_
# define PLAYER_HPP_

# include <vector>

# include "BoardValue.hpp"

class Player
{
protected:
    explicit Player(const BoardValue & value);

public:
    static std::vector<Player> newPlayers(unsigned int nb);

    Player(const Player & o);
    Player & operator=(const Player & o);
    virtual ~Player();

    unsigned int getColor() const;
    unsigned int getID() const;
    unsigned int getPosX() const;
    unsigned int getPosY() const;

    void setPosX(unsigned int posX);
    void setPosY(unsigned int posY);
    void setPos(unsigned int posX, unsigned int posY);

protected:
    BoardValue   _value;
    unsigned int _posX;
    unsigned int _posY;
};

#endif // !PLAYER_HPP_
