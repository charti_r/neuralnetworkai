#ifndef BOARD_HPP_
# define BOARD_HPP_

# include <iostream>

# include "Arr2.hpp"
# include "BoardValue.hpp"
# include "Player.hpp"

class Board : public Arr2<BoardValue>
{
public:
    explicit Board(unsigned int sizex, unsigned int sizey);
    Board(const Board & o);
    Board & operator=(const Board & o);
    virtual ~Board();

    unsigned int count(const Player & player) const;
    void clear();
};

std::ostream & operator<<(std::ostream & os, const Board & board);

#endif // !BOARD_HPP_
