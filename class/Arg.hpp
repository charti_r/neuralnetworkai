#ifndef ARG_H_
# define ARG_H_

# include <vector>
# include <string>

class Arg
{
public:
    explicit Arg(int ac, char ** av);
    Arg(const Arg & o);
    Arg & operator=(const Arg & o);
    virtual ~Arg();

    unsigned int size() const;
    const std::string & operator[](unsigned int index) const;

private:
    std::vector<std::string> _val;
};

#endif // !ARG_H_
