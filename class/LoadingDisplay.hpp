#ifndef LOADING_DISPLAY_HPP_
# define LOADING_DISPLAY_HPP_

# include <iostream>

class LoadingDisplay
{
public:
    explicit LoadingDisplay(unsigned int size = 100);
    LoadingDisplay(const LoadingDisplay & o);
    LoadingDisplay & operator=(const LoadingDisplay & o);
    virtual ~LoadingDisplay();

    void  add(unsigned int nb = 1);
    float percent(unsigned int on = 100) const;
    void  reset();
    void  reset(unsigned int size);

    unsigned int count() const;
    void         setCount(unsigned int count);
    unsigned int size() const;
    void         setSize(unsigned int size);

private:
    unsigned int _count;
    unsigned int _size;
};

std::ostream & operator<<(std::ostream & os, const LoadingDisplay & ld);

#endif // !LOADING_DISPLAY_HPP_
