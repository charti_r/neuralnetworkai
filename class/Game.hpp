#ifndef AGAME_HPP_
# define AGAME_HPP_

# include <iostream>
# include <vector>
# include <string>

# include <cstdlib>
# include <ctime>
# include <unistd.h>

# include <SFML/System.hpp>

# include "LibNeuronNetwork.hpp"

# include "Board.hpp"
# include "Player.hpp"
# include "NetworkData.hpp"

class Game
{
private:
    typedef void (Game::*funcPtr)(Player &);

public:
    static constexpr float maxSpeed = 1000000.f;

public:
    explicit Game(unsigned int nbPlayer);
    Game(const Game &) = delete;
    Game & operator=(const Game &) = delete;
    virtual ~Game();

    bool init(const NetworkData * first, const NetworkData * last);
    std::vector<unsigned int> run(unsigned int nbTurn);

    unsigned int getNbPlayer() const;
    void         setSpeed(float speed);
    void         upSpeed(float upSpeed);
    void         downSpeed(float downSpeed);
    float        getSpeed() const;

protected:
    virtual bool onInit();
    virtual void handleEvent();
    virtual void displayBoard();

private:
    void playPlayers();
    void moveW(Player & p);
    void moveA(Player & p);
    void moveS(Player & p);
    void moveD(Player & p);

protected:
    const unsigned int _nbPlayer;

    Board                             _board;
    std::vector<unsigned int>         _score;
    std::vector<Player>               _players;
    std::vector<NeuronNetwork>        _networks;
    std::vector<std::list<ANeuron *>> _neuronInputs;

    unsigned int _turn;
    unsigned int _nbTurn;
    sf::Clock    _clock;
    float        _speed;

    std::map<unsigned int, funcPtr> _action;
};

#endif // !AGAME_HPP_
