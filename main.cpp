#include "GenAI.hpp"

int main(int ac, char ** av)
{
	GenAI genAI;

	if (genAI.init(ac, av) == false)
		return 1;
	return genAI.run();
}
